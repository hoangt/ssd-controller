/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2013,2014  Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

////////////////////////////////////////////////////////////////////////////////
// Name of the Module	: NVM Controller model
// Coded by		: M S Santosh Kumar 
// Email Id             : ee10b017@ee.iitm.ac.in
// Module Description	: This module simulates NVM controller to test pcie controller
//
//Functionality at a glance :
//     This module is to used in conjunction with Pcie controller for testing
//  
//     1. Store data to/transfer data from bram from/to pcie controller
//     2. Provides a set of host control registers to initiate data transfer
//								
////////////////////////////////////////////////////////////////////////////////

`define NTESTS 1

package NvmController_model;

import Vector::*;
import BuildVector::*;
import DReg::*;
import global_defs_tb::*;
import ConfigReg::*;
import BRAM::*;

//simulates NvmController configuration interface
interface Ifc_config;
   method Action _write(Bit#(32) _address, Bit#(64) _data, Bool dword_aligned);
   method Bit#(64) read_(Bit#(32) address);
   method Action _write_header(Bit#(32) _header);
endinterface

//simulates NvmController interface 
interface Ifc_Controller_Rx_Tb;
   interface Ifc_config ifc_config;
   interface NvmTransmitToWrapper_Interface nvmTransmitToPCIe_interface;
   interface Ifc_completion ifc_completion;	
endinterface
   

/* random test data (not used in this version)

Vector#(5,Bit#(128)) vc_tx_data = vec( 
   128'h1C0DEDEAD,128'h2C0DEDEAD,128'h3C0DEDEAD,128'h4C0DEDEAD,128'h5C0DEDEAD );

Vector#(5,UInt#(64)) vc_tx_address = vec(
   64'hADD1,64'hADD2,64'hADD3,64'hADD4,64'hADD5);

Vector#(5,bit) vc_tx_valid = vec(
   1'b1,1'b1,1'b0,1'b1,1'b0);		
*/			

module mkNvmController_model(Ifc_Controller_Rx_Tb);

   /*-----------------Reg and Wire declarations : start---------------------*/
   
   
   /***************** Host Control registers for initiating data transfer *******************/
  
   Reg#(Bit#(32)) rg_dma_address_lsb <- mkConfigReg(0); //holds MSW of target transfer address
   Reg#(Bit#(32)) rg_dma_address_msb <- mkConfigReg(0); //holds LSW of target transfer address
   Reg#(Bit#(1)) drg_dma_start <- mkDReg(0); //data transfer starts when this register is asserted
   Reg#(bit) rg_write <- mkConfigReg(0); //indicates read/write
   Reg#(Bit#(4)) i <- mkReg(0); //index
   Reg#(Bit#(32)) rg_data_transfer_count <- mkReg(0); //counts the number of transfered cycles
 
   //Each warp is a single data transfer request
   Reg#(UInt#(32)) rg_num_warps <- mkReg(0); // stores the number of warps
   Reg#(UInt#(32)) rg_num_warps_compl <- mkReg(0); //stores the expected number of completion warps
   
   /********************************************************************************************/

   
   Reg#(Bit#(128)) rg_out_data_to_pcie <- mkRegU();  // Data to be sent to PCIe
   Reg#(UInt#(64)) rg_out_address_to_pcie <- mkRegU();  // Address being sent to PCIe
   Reg#(Bit#(16)) rg_out_tag_to_pcie <- mkRegU();  // the tag being requested
   Reg#(Bit#(32)) rg_out_payload_length <- mkRegU();  // Length of payload in DWords
   Reg#(bit)  rg_out_write_to_pcie <- mkRegU();  // request to send Write TLP
   Reg#(bit) drg_out_data_valid <- mkDReg(1'b0);  // Indicates that the data being sent is valid
   Wire#(bit) dwr_wait <- mkDWire(0);  // Wait assertion from the PCIe controller
   Reg#(bit) rg_transfer_progress_valid <- mkConfigReg(0); //indicates the transfer is in progress
   Reg#(UInt#(8)) rg_bram_address <- mkConfigReg(0); //bram address from which the data will be read for transfer
   
   Vector#(32, Reg#(Bit#(32))) rg_header_mem <- replicateM(mkReg(0)); //used for debugging
   Reg#(Bit#(TLog#(32))) rg_header_ptr <- mkReg(0); //used for debugging
   Reg#(Bit#(32)) rg_dts_length <- mkConfigReg(0); //holds transfer length
   Reg#(UInt#(8)) rg_bram_address_write <- mkReg(0); //bram address to which data will be written
   Reg#(Bit#(32)) rg_compl_length <- mkReg(0); //holds expected completion length
   
   //These registers are used for speed test
   Reg#(Bit#(32)) rg_dts_counter <- mkConfigReg(0); //counts the elapsed cycles since request
   Reg#(Bool) rg_run_counter <- mkConfigReg(False); //indicates the counter is running
   Reg#(Bool) drg_reset_counter <- mkDReg(False); // resets the counter
   
   
   BRAM2Port#(UInt#(8), Bit#(128)) bram_store_data <-(
      mkBRAM2Server(defaultValue)); //used to store the received transfer data

   /*---------------------Reg and Wire declarations : end -------------------------------*/
   
   /*-------------------Function declarations: start ------------------------------------*/
   
   function BRAMRequest#(UInt#(8), Bit#(128)) makeRequest(
      Bool write, UInt#(8) addr, Bit#(128) data);
      /* Create bram request structure. */
      
      return BRAMRequest{
	 write: write,
	 responseOnWrite:False,
	 address: addr,
	 datain: data
	 };
   endfunction

   /*-----------------------Function declarations: end -----------------------------------*/
   
   /*-------------------------rule declarations: start------------------------------------*/
   
   // counts the elapsed cycles since request
   (*fire_when_enabled,no_implicit_conditions*)
   rule rl_count_cycles(rg_run_counter);
      
      rg_dts_counter <= rg_dts_counter + 1;
      
   endrule
   
   // resets the counter when drg_reset_counter is asserted and counter is not running
   rule rl_reset_counter (drg_reset_counter && !rg_run_counter );
      
      rg_dts_counter <= 32'h0;
   
   endrule
   
   // transmits the data from and to the bram
   rule rl_transmit_data(drg_dma_start == 1'b1 || rg_transfer_progress_valid == 1'b1);
      
      Bit#(32) lv_transfer_limit; //indicates number of cycles required to complete request
      
      // read requires only one cycle while write cycles depends on length of data transfer 
      lv_transfer_limit = rg_write == 1'b1 ? ((rg_dts_length/4) -1) : 32'h0;
      $display("%d: rl_transmit_data fires: NvmController_model: lv_transfer_limit: 0x%x\n",$stime,lv_transfer_limit);
      
      if(dwr_wait == 1'b0) // if pcie is ready
	 begin
	    $display("%d: j value: 0x%x",$stime,rg_data_transfer_count);
	    rg_out_write_to_pcie <= rg_write;
	    rg_out_tag_to_pcie <= truncate(pack(rg_num_warps)); //unique tag is provided by warp number
	    rg_out_payload_length <= rg_dts_length;
	    if (rg_data_transfer_count == 0)
	       rg_out_address_to_pcie <= unpack({rg_dma_address_msb, rg_dma_address_lsb});
	    else
	       rg_out_address_to_pcie <= rg_out_address_to_pcie + 16;
	    drg_out_data_valid <= 1'b1;
	    // (not used) rg_out_data_to_pcie <= vc_tx_data[rg_data_transfer_count%5];
	  
	    // bram has one cycle latency. request one cycle earlier
	    let lv_dram_data <- bram_store_data.portA.response.get();
	    rg_out_data_to_pcie <= lv_dram_data;
	    if(rg_data_transfer_count == lv_transfer_limit)
	       begin
		  rg_data_transfer_count <= 0;
		  rg_transfer_progress_valid <= 0;
		  //if number of warps is not zero. send another transfer
		  if (rg_num_warps != 0)
		     begin
			drg_dma_start <= 1'b1;
			rg_num_warps <= rg_num_warps - 1;
			bram_store_data.portA.request.put(makeRequest(False,0,0));
			rg_bram_address <= 1;
		     end
		  else
		     begin
			rg_bram_address <= 0;
		     end
	       end
	    else 
	       begin
		  rg_data_transfer_count <= rg_data_transfer_count + 1;
		  rg_transfer_progress_valid <= 1;
		  rg_bram_address <= rg_bram_address + 1;
		  bram_store_data.portA.request.put(
		     makeRequest(False,rg_bram_address,0));
	       end
	 end
      else
	 begin
	    drg_out_data_valid <= 1;
	    rg_transfer_progress_valid <= 1;
	 end
   endrule:rl_transmit_data
	
   /*-----------------------rule declarations: end---------------------------*/
   
   /*-----------------------Interface declarations: start---------------------*/
   
   //nvme to pcie interface
   interface nvmTransmitToPCIe_interface = fn_nvmTransmitToPCIe_interface(
      rg_out_data_to_pcie,
      rg_out_address_to_pcie,
      rg_out_tag_to_pcie,
      rg_out_write_to_pcie,
      rg_out_payload_length,
      drg_out_data_valid,
      dwr_wait
      );
   
   //completion interface
   interface Ifc_completion ifc_completion;
      method Action _write(Bit#(128) _data_dword, Bit#(16) _tag);
	 $display("%d: Ifc_completion",$stime());
	 //store the received data in bram
	 bram_store_data.portB.request.put(
	    makeRequest(True,rg_bram_address_write,_data_dword));
	 
	 // if bram address is equal to the expected completion length, then reset bram address
	 if (rg_bram_address_write == unpack(truncate(rg_compl_length - 1)))
	  begin
	     rg_bram_address_write <= 0;
	     if (rg_num_warps_compl == 0)
		begin
		   // set the run counter to false if expected warps are received
		   rg_run_counter <= False;
		end
	     else
		begin
		   // else decrement the number of expected warps
		   rg_num_warps_compl <= rg_num_warps_compl - 1;
		end
	  end
	 else
	    rg_bram_address_write <= rg_bram_address_write + 1;
      endmethod
   endinterface
   

   // configuration interface
   // The host writes to the registers in this interface to control the data transfer    
   interface Ifc_config ifc_config;
      
      // Though the configuration interface simulates the nvm controller, the
      // registers defined here are NOT same as the ones in NvmController.
      // These registers are used for testing the pcie controller.
      // Also note that the read and write address DONOT map to same registers.

      method Action _write(Bit#(32) _address, Bit#(64) _data, dword_aligned) 
	 if (rg_transfer_progress_valid == 1'b0 && drg_dma_start == 1'b0);

	 /* Method for writing into the Controller registers */
   
	 $display("%d: ifc_config._write: Configuration Write of 0x%x to 0x%x",
			$stime(), _data, _address);
	 case (_address)
	    32'h00: //LSW of the address to/from which transfer happens
	    rg_dma_address_lsb <= _data[31:0];
	    32'h04: //MSW of the address to/from which transfer happens
	    rg_dma_address_msb <= _data[31:0];
	    32'h08: //length of the data transfer
	    rg_dts_length <= _data[31:0];
	    // write of 0x1 to this register initiates read
	    // write of 0x3 to this register initiates write.
	    // any other write to this register is not valid.
	    // This address has to be written only after the address register, dts_length
	    // register and num_warps register are set.
	    32'h0c:
	    begin
	       drg_dma_start <= _data[0];
	       rg_write <= _data[1];
	       if (_data[1] == 1'b0)
	       begin
		  rg_run_counter <= True;
		  rg_compl_length <= (rg_dts_length >> 2);
	       end
	       $display("%d: NvmController_model: bram_store_data_put\n",$stime());
	       bram_store_data.portA.request.put(makeRequest(False,rg_bram_address,0));
	       rg_bram_address <= rg_bram_address + 1;
	    end
	   32'h10:
	    // write to this register resets the speed counter
	    drg_reset_counter <= True;
	   32'h14:
	    // this register has to be written only if it is a read from Nvme
	    // If it is a write and the register is written, the behaviour is undefined.
	    begin
	       rg_num_warps <= unpack(_data[31:0])-1;
	       rg_num_warps_compl <= unpack(_data[31:0])-1;
	    end
	 endcase
      endmethod
   
      method Bit#(64) read_(Bit#(32) address);
	 case(address)
	    32'h00: //returns the LSW dma address
	    return {32'h0,rg_dma_address_lsb};
	    32'h04: //returns the MSW dma address
	    return {32'h0,rg_dma_address_msb};
	    32'h08: //returns transfer length
	    return {32'h0,rg_dts_length};
	    32'h0c: //returns the value of speed counter
	    return {32'h0,rg_dts_counter};
	    32'h10: //returns the status of speed counter
	    return {63'h0,pack(rg_run_counter)};
	    32'h14: //returns the bram address read
	    return {56'h0,pack(rg_bram_address)};
	    32'h18: //returns the bram address write
	    return {56'h0,pack(rg_bram_address_write)};
	    32'h1c: //returns the expected completions length
	    return extend(pack(rg_compl_length));
	    32'h20: //returns the expected completion warps
	    return extend(pack(rg_num_warps_compl));
	    32'h24: //returns the set number of warps
	    return extend(pack(rg_num_warps));
 	    default: begin
			// few debug registers write from pcie controller are mapped here
			// used only for debugging
			return {32'h0, rg_header_mem[(address - 'h28) >> 2]};
		     end
	 endcase
      endmethod
      
      // this method is written by the pcie controller. used for debugging
      method Action _write_header(Bit#(32) _header);
	 rg_header_mem[rg_header_ptr] <= _header;
	 rg_header_ptr <= rg_header_ptr + 1;
      endmethod	
   
   endinterface
   
endmodule

endpackage
