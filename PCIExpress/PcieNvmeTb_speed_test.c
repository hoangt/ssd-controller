#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/pci.h>
#include <linux/io.h>
#include <linux/delay.h>

MODULE_LICENSE("GPL");

#define VENDOR_ID 0x10EE
#define DEVICE_ID 0x7024

// initialize the device id structure
static struct pci_device_id  pci_device_id_DevicePCI[] =
{
  {VENDOR_ID, DEVICE_ID, PCI_ANY_ID, PCI_ANY_ID, 0, 0, 0},
  {}	// end of list
};

int device_probe(struct pci_dev *dev, const struct pci_device_id *id)
{
	int ret;
	int i = 0;
	int j = 0;
	unsigned long addr; //bar0 address
	unsigned long phy_addr; 
	uint32_t *baseptr;
	unsigned long length;
	uint32_t data_readq1;
	uint32_t data_readq2;
	uint32_t speed_counter[100];
	uint32_t data_read_temp;
	uint32_t run_counter;
	uint32_t bram_address;
	uint32_t num_warps_cmpl;
	dma_addr_t dma_handle,dma_handle1;
	dma_addr_t dmaHWaddr,dmaHWaddr1;
	void *dma_virt_addr,*dma_virt_addr1;
	struct device *dmadev = &dev->dev;	
	uint32_t *dma_value,*dma_value1;
	
	ret = pci_enable_device(dev);
	if (ret < 0)
	{
		printk(KERN_WARNING "PcieNvmeTb: unable to initialize PCI device\n");
		return ret;
	}

	ret = pci_request_regions(dev, "PcieNvmeTb");
	if (ret < 0)
	{
		printk(KERN_WARNING "PcieNvmeTb: unable to reserve PCI resources\n");
		pci_disable_device(dev);
		return ret;
	}

	pci_set_master(dev);
	if (ret < 0)
	addr = pci_resource_start(dev,0);
	length = pci_resource_len(dev,0);
	baseptr = pci_iomap(dev,0,length);
	
	dma_virt_addr = pci_alloc_consistent(dev,4096,&dma_handle);
	dmaHWaddr = pci_map_single(dev,dma_virt_addr,4096,PCI_DMA_FROMDEVICE);
	dma_virt_addr1 = pci_alloc_consistent(dev,4096,&dma_handle1);
	dmaHWaddr1 = pci_map_single(dev,dma_virt_addr1,4096,PCI_DMA_FROMDEVICE);
	dma_value = (uint32_t*) dma_virt_addr;
	dma_value1 = (uint32_t*) dma_virt_addr1;

	//write some data into buffer
	for (i = 0; i < 1024; i++)
	  {
	    *(dma_value + i) = i;
	  }

	//wait
	msleep(1000);

	//reading config registers for checking validity
	run_counter = readl(baseptr+4);
	bram_address = readl(baseptr+5);
	printk(KERN_INFO "run_counter: %d\n",run_counter);
	printk(KERN_INFO "bram_address: %d\n",bram_address);
	
	for (j = 0; j < 1; j++)
	  {
	    writel(0x00,baseptr+4);   //reset the speed counter
	    msleep(1000);
	    writel(dmaHWaddr,baseptr); //LSW address
	    writel(0x00,baseptr+1);   //MSW address
	    writel(0x400,baseptr+2); //payload length
	
	    //this write specifies the number of warps. we are reading
	    //from same memory location in all the warps
	    writel(j,baseptr+5);
	    
	    writel(0x01,baseptr+3); //start read by Nvme
	    
	    //wait
	    msleep(3000);
	    speed_counter[j] = readl(baseptr+3); //read from hardware speed counter
	  }

	for (j = 0; j < 1; j++)
	  {
	    printk(KERN_INFO "%d %x\n",j,speed_counter[j]);
	  }

	printk(KERN_INFO "PcieNvmeTb: device_probe successful\n");
	iounmap(baseptr);
	//dma_free_coherent(dmadev,4096,dma_virt_addr,dma_handle);
	return ret;
}

//remove device
void device_remove(struct pci_dev *dev)
{
	pci_release_regions(dev);
	pci_disable_device(dev);
	printk(KERN_INFO "PcieNvmeTb: device removed\n");

}

struct pci_driver  pci_driver_DevicePCI =
{
	name: "PcieNvmeTb",
	id_table: pci_device_id_DevicePCI,
	probe: device_probe,
	remove: device_remove
};

static int init_module_DevicePCI(void)
{
	printk(KERN_INFO "PcieNvmeTb: init\n");
	return pci_register_driver(&pci_driver_DevicePCI);
}

void cleanup_module_DevicePCI(void)
{
	printk(KERN_INFO "PcieNvmeTb: cleanup\n");
	pci_unregister_driver(&pci_driver_DevicePCI);
}


module_init(init_module_DevicePCI);
module_exit(cleanup_module_DevicePCI);
