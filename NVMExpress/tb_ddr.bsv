/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
module name: Testbench for DDR with Xilinx AXI Datamover
author name: Maximilian Singh
email id: maximilian.singh@student.kit.edu
last update done on 12th June 2014

This testbench test the DDR with Xilinx AXI Datamover.

*/

package tb_ddr;

import DReg::*;
import ConfigReg::*;
import FIFO::*;

import global_definitions::*;

typedef enum {
   IDLE,
   REQ_READ,
   READ,
   REQ_WRITE,
   WRITE,
   WAIT_FOR_WRITE,
   REQ_RW,
   RW,
   WAIT_FOR_RW
   } Tb_state_type deriving (Bits, Eq);

/*
This type contains the information which is sent to the command ports (s2mm and
mm2s) of the Xilinx AXI Datamover IP to start the transmission.
*/
typedef struct {
   UInt#(32) address;
   UInt#(23) length;
   } Ddr_info deriving (Bits, Eq);


interface Ifc_tb_for_ddr;
   interface Ifc_read_data_ddr ifc_read_data_ddr;
   interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
   interface Ifc_write_data_ddr ifc_write_data_ddr;
   interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
   interface Ifc_write_sts_ddr ifc_write_sts_ddr;
   method Action _reg_in(Bit#(32) _data);
   method Bit#(32) reg_out_();
   method Bit#(8) leds_();
endinterface

(* synthesize *)
(* always_ready *)
(* always_enabled *)
module mkTb_for_ddr(Ifc_tb_for_ddr);

   Reg#(Tb_state_type) rg_tb_state <- mkReg(IDLE);
   Reg#(UInt#(32)) rg_count <- mkReg(0);

   Reg#(Bit#(32)) rg_last_data <- mkReg(0);
   Reg#(UInt#(21)) rg_nr_dwords <- mkReg('h1024);
   Reg#(Bit#(2)) rg_counter_sel <- mkReg(0);
    Reg#(Bool) drg_restart <- mkDReg(False);

   Reg#(Bit#(8)) rg_out_leds <- mkReg('hAA);

   /* Counter which can be used by the software. */
   // clk frequency of 100 MHz is assumed
   Reg#(Bit#(64)) rg_read_ns_count <- mkReg(0);
   Reg#(Bool) rg_read_count_started <- mkReg(False);
   // clk frequency of 100 MHz is assumed
   Reg#(Bit#(64)) rg_write_ns_count <- mkReg(0);
   Reg#(Bool) rg_write_count_started <- mkReg(False);
   // clk frequency of 100 MHz is assumed
   Reg#(Bit#(64)) rg_rw_ns_count <- mkReg(0);
   Reg#(Bool) rg_rw_count_started <- mkReg(False);

   /* Wires of the Read Data interface of the Xilinx AXI Datamover IP */
   Wire#(bit) wr_in_read_data_ddr_valid <- mkWire();
   Wire#(Bit#(32)) wr_in_read_data_ddr_data <- mkWire();
   Wire#(bit) wr_in_read_data_ddr_last <- mkWire();
   Wire#(bit) dwr_out_read_data_ready <- mkDWire(1'b0);

   /* Wires of the Read Command interface of the Xilinx AXI Datamover IP */
   Reg#(bit) rg_out_read_cmd_ddr_valid <- mkConfigReg(0);
   Wire#(bit) wr_in_read_cmd_ddr_ready <- mkWire();
   Reg#(UInt#(32)) rg_out_ddr_read_addr <- mkReg(0);
   Reg#(UInt#(23)) rg_out_ddr_read_length <- mkReg(0);

   /* Wires of the Write Data interface of the Xilinx AXI Datamover IP */
   Reg#(bit) rg_out_write_data_ddr_valid <- mkReg(0);
   Reg#(Bit#(32)) rg_out_write_data_ddr_data <- mkReg(0);
   Reg#(bit) rg_out_write_data_ddr_last <- mkReg(0);
   Wire#(bit) wr_in_write_data_ddr_ready <- mkWire();

   /* Wires of the Write Command interface of the Xilinx AXI Datamover IP */
   Reg#(bit) rg_out_write_cmd_ddr_valid <- mkReg(0);
   Wire#(bit) wr_in_write_cmd_ddr_ready <- mkWire();
   Reg#(UInt#(32)) rg_out_ddr_write_addr <- mkReg(0);
   Reg#(UInt#(23)) rg_out_ddr_write_length <- mkReg(0);

   /* Wires of the Write Status interface of the Xilinx AXI Datamover IP */
   Reg#(bit) drg_out_write_sts_ddr_ready <- mkDReg(1'b0);
   Wire#(bit) wr_in_write_sts_ddr_valid <- mkWire();
   Wire#(Bit#(8)) wr_in_write_sts_ddr_data <- mkWire();

   Reg#(Bool) rg_write_data_done <- mkConfigReg(False);
   Reg#(Bool) rg_write_sts_done <- mkConfigReg(False);
   
   Reg#(Bool) rg_rw_data_done <- mkConfigReg(False);
   Reg#(Bool) rg_rw_sts_done <- mkConfigReg(False);
   
   FIFO#(Bit#(32)) ff_rw <- mkSizedFIFO(5);
   
   rule rl_read_ns_count;
       if (rg_read_count_started)
	 begin
	    if (rg_tb_state == REQ_WRITE)
	       rg_read_count_started <= False;
	    else
	       rg_read_ns_count <= rg_read_ns_count + 10;
	 end
      else if (rg_tb_state == IDLE && drg_restart == True)
	 begin
	    rg_read_ns_count <= 0;
	    rg_read_count_started <= True;
	 end
   endrule: rl_read_ns_count


   rule rl_write_ns_count;
      if (rg_write_count_started)
	 begin
	    if (rg_tb_state == REQ_RW)
	       rg_write_count_started <= False;
	    else
	       rg_write_ns_count <= rg_write_ns_count + 10;
	 end
      else if (rg_tb_state == REQ_WRITE)
	 begin
	    rg_write_ns_count <= 0;
	    rg_write_count_started <= True;
	 end
   endrule: rl_write_ns_count


   rule rl_rw_ns_count;
      if (rg_rw_count_started)
	 begin
	    if (rg_tb_state == IDLE && drg_restart == False)
	       rg_rw_count_started <= False;
	    else
	       rg_rw_ns_count <= rg_rw_ns_count + 10;
	 end
      else if (rg_tb_state == REQ_RW)
	 begin
	    rg_rw_ns_count <= 0;
	    rg_rw_count_started <= True;
	 end
   endrule: rl_rw_ns_count



   rule rl_tb_idle_state (rg_tb_state == IDLE);
      rg_write_data_done <= False;
      rg_write_sts_done <= False;
      rg_rw_data_done <= False;
      rg_rw_sts_done <= False;
      if (drg_restart == True)
	 begin
	    rg_out_leds <= 1;
	    rg_tb_state <= REQ_READ;
	 end
   endrule: rl_tb_idle_state

   rule rl_tb_request_read_state (rg_tb_state == REQ_READ);
      rg_out_read_cmd_ddr_valid <= 1'b1;
      rg_out_ddr_read_addr <= 32'h01200000;
      rg_out_ddr_read_length <= extend(rg_nr_dwords) * 4;
      rg_tb_state <= READ;
   endrule: rl_tb_request_read_state

   rule rl_tb_read_state0 (rg_tb_state == READ &&
			   wr_in_read_cmd_ddr_ready == 1'b1);
      rg_out_read_cmd_ddr_valid <= 1'b0;
   endrule: rl_tb_read_state0

   rule rl_tb_read_state1 (rg_tb_state == READ &&
			   wr_in_read_data_ddr_valid == 1'b1);
      dwr_out_read_data_ready <= 1'b1;
      if (wr_in_read_data_ddr_data != pack(rg_count))
	 rg_out_leds <= 3;
      if (wr_in_read_data_ddr_last == 1'b1)
	 begin
	    rg_count <= 0;
	    rg_tb_state <= REQ_WRITE;
	 end
      else
	 rg_count <= rg_count + 1;
   endrule: rl_tb_read_state1

   rule rl_tb_request_write_state (rg_tb_state == REQ_WRITE);
      rg_out_write_cmd_ddr_valid <= 1'b1;
      rg_out_ddr_write_addr <= 32'h01300000;
      rg_out_ddr_write_length <= extend(rg_nr_dwords) * 4;
      rg_tb_state <= WRITE;
   endrule: rl_tb_request_write_state

   rule rl_tb_write_state0 (rg_tb_state == WRITE &&
			    wr_in_write_cmd_ddr_ready == 1'b1);
      rg_out_write_cmd_ddr_valid <= 1'b0;
   endrule: rl_tb_write_state0

   rule rl_tb_write_state1 (rg_tb_state == WRITE &&
			   wr_in_write_data_ddr_ready == 1'b1);
      rg_out_write_data_ddr_data <= pack(rg_count);
      if (rg_count == extend(rg_nr_dwords) - 1)
	 begin
	    rg_count <= 0;
	    rg_out_write_data_ddr_last <= 1'b1;
	    rg_tb_state <= WAIT_FOR_WRITE;
	 end
      else
	 rg_count <= rg_count + 1;
      rg_out_write_data_ddr_valid <= 1'b1;
   endrule: rl_tb_write_state1

   rule rl_tb_wait_for_write0 (rg_tb_state == WAIT_FOR_WRITE &&
			       wr_in_write_data_ddr_ready == 1'b1);
      rg_out_write_data_ddr_valid <= 1'b0;
      rg_out_write_data_ddr_last <= 1'b0;
      rg_write_data_done <= True;
   endrule: rl_tb_wait_for_write0

   rule rl_tb_wait_for_write1 (rg_tb_state == WAIT_FOR_WRITE);
      drg_out_write_sts_ddr_ready <= 1'b1;
      if (wr_in_write_sts_ddr_valid == 1'b1)
	 rg_write_sts_done <= True;
   endrule: rl_tb_wait_for_write1

   rule rl_tb_wait_for_write2 (rg_tb_state == WAIT_FOR_WRITE &&
			       rg_write_data_done &&
			       rg_write_sts_done);
      rg_tb_state <= REQ_RW;
   endrule: rl_tb_wait_for_write2

   rule rl_tb_request_rw (rg_tb_state == REQ_RW);
      rg_out_read_cmd_ddr_valid <= 1'b1;
      rg_out_ddr_read_addr <= 32'h01300000;
      rg_out_ddr_read_length <= extend(rg_nr_dwords) * 4;

      rg_out_write_cmd_ddr_valid <= 1'b1;
      rg_out_ddr_write_addr <= 32'h01400000;
      rg_out_ddr_write_length <= extend(rg_nr_dwords) * 4;
      rg_tb_state <= RW;
   endrule: rl_tb_request_rw

   rule rl_tb_rw0 (rg_tb_state == RW && wr_in_read_cmd_ddr_ready == 1'b1);
      rg_out_read_cmd_ddr_valid <= 1'b0;
   endrule: rl_tb_rw0

   rule rl_tb_rw1 (rg_tb_state == RW && wr_in_write_cmd_ddr_ready == 1'b1);
      rg_out_write_cmd_ddr_valid <= 1'b0;
   endrule: rl_tb_rw1

   rule rl_tb_rw2 (rg_tb_state == RW && wr_in_read_data_ddr_valid == 1'b1);
      dwr_out_read_data_ready <= 1'b1;
      //ff_rw.enq(wr_in_read_data_ddr_data);
   endrule: rl_tb_rw2
   
   rule rl_tb_rw3 (rg_tb_state == RW && wr_in_write_data_ddr_ready == 1'b1);
      rg_out_write_data_ddr_data <= pack(rg_count); //ff_rw.first();
      if (rg_count == extend(rg_nr_dwords) - 1)
	 begin
	    rg_count <= 0;
	    rg_out_write_data_ddr_last <= 1'b1;
	    rg_tb_state <= WAIT_FOR_RW;
	 end
      else
	 rg_count <= rg_count + 1;
      //ff_rw.deq();
      rg_out_write_data_ddr_valid <= 1'b1;
   endrule: rl_tb_rw3

   rule rl_tb_wait_for_rw0 (rg_tb_state == WAIT_FOR_RW &&
			    wr_in_write_data_ddr_ready == 1'b1);
      rg_out_write_data_ddr_valid <= 1'b0;
      rg_out_write_data_ddr_last <= 1'b0;
      rg_rw_data_done <= True;
   endrule: rl_tb_wait_for_rw0

   rule rl_tb_wait_for_rw1 (rg_tb_state == WAIT_FOR_RW);
      drg_out_write_sts_ddr_ready <= 1'b1;
      if (wr_in_write_sts_ddr_valid == 1'b1)
	 rg_rw_sts_done <= True;
   endrule: rl_tb_wait_for_rw1

   rule rl_tb_wait_for_rw2 (rg_tb_state == WAIT_FOR_RW && rg_rw_data_done &&
			    rg_rw_sts_done);
      rg_tb_state <= IDLE;
   endrule: rl_tb_wait_for_rw2




   /* --- Interfaces for the connection to the Xilinx AXI Datamover IP. --- */
   /* Interface to read payload data from Xilinx AXI Datamover IP. */
   interface Ifc_read_data_ddr ifc_read_data_ddr;
      method bit ready_();
	 return dwr_out_read_data_ready;
      endmethod: ready_

      method Action _valid(bit _is_valid);
	 wr_in_read_data_ddr_valid <= _is_valid;
      endmethod: _valid

      method Action _data_in(Bit#(32) _data);
	 wr_in_read_data_ddr_data <= _data;
      endmethod: _data_in

      method Action _last(bit _is_last);
	 wr_in_read_data_ddr_last <= _is_last;
      endmethod: _last
   endinterface: ifc_read_data_ddr


   /* Interface to send read request to the Xilinx AXI Datamover IP. */
   interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
      method bit valid_();
	 return rg_out_read_cmd_ddr_valid;
      endmethod: valid_

      method Bit#(72) data_();
	 return {
	    4'b0,  // reserved
	    4'b0,  // command tag; not used
	    pack(rg_out_ddr_read_addr),
	    1'b0,  // DRE ReAlignment Request; not used
	    1'b1,  // end of frame
	    6'b0	,  // DRE Stream Alignment; not used
	    1'b1,  // INCR access type
	    pack(rg_out_ddr_read_length)  // bytes to transfer
	    };
      endmethod: data_

      method Action _ready(bit _is_ready);
	 wr_in_read_cmd_ddr_ready <= _is_ready;
      endmethod: _ready
   endinterface: ifc_read_cmd_ddr


   /* Interface to write payload data to the Xilinx AXI Datamover IP. */
   interface Ifc_write_data_ddr ifc_write_data_ddr;
      method bit valid_();
	 return rg_out_write_data_ddr_valid;
      endmethod: valid_

      method Bit#(32) data_out_();
	 return rg_out_write_data_ddr_data;
      endmethod: data_out_

      method bit last_();
	 return rg_out_write_data_ddr_last;
      endmethod: last_

      method Action _ready(bit _is_ready);
	 wr_in_write_data_ddr_ready <= _is_ready;
      endmethod: _ready
   endinterface: ifc_write_data_ddr


   /* Interface to send write request to the Xilinx AXI Datamover IP. */
   interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
      method bit valid_();
	 return rg_out_write_cmd_ddr_valid;
      endmethod: valid_

      method Bit#(72) data_();
	 return {
	    4'b0,  // reserved
	    4'b0,  // command tag; not used
	    pack(rg_out_ddr_write_addr),
	    1'b0,  // DRE ReAlignment Request; not used
	    1'b1,  // end of frame
	    6'b0,  // DRE Stream Alignment; not used
	    1'b1,  // INCR access type
	    pack(rg_out_ddr_write_length)  // bytes to transfer
	    };
      endmethod: data_

      method Action _ready(bit _is_ready);
	 wr_in_write_cmd_ddr_ready <= _is_ready;
      endmethod: _ready
   endinterface: ifc_write_cmd_ddr

   /* Interface to receive write status from the Xilinx AXI Datamover IP. */
   interface Ifc_write_sts_ddr ifc_write_sts_ddr;
      method bit ready_();
	 return drg_out_write_sts_ddr_ready;
      endmethod: ready_

      method Action _valid(bit _is_valid);
	 wr_in_write_sts_ddr_valid <= _is_valid;
      endmethod: _valid

      method Action _data_in(Bit#(8) _data);
	 wr_in_write_sts_ddr_data <= _data;
      endmethod: _data_in
   endinterface: ifc_write_sts_ddr




   method Action _reg_in(Bit#(32) _data);
      if (_data == rg_last_data + 1)
	    drg_restart <= True;
      else if (_data != rg_last_data)
	 begin
	    rg_nr_dwords <= unpack(_data[20:0]);
	    rg_counter_sel <= _data[31:30];
	 end
      rg_last_data <= _data;
   endmethod: _reg_in

   method Bit#(32) reg_out_();
      if (rg_counter_sel == 0)
	 return rg_read_ns_count[31:0];
      else if (rg_counter_sel == 1)
	 return rg_write_ns_count[31:0];
      else
	 return rg_rw_ns_count[31:0];
   endmethod: reg_out_

   method Bit#(8) leds_();
      return rg_out_leds;
   endmethod: leds_

endmodule: mkTb_for_ddr
endpackage: tb_ddr
