/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2013,2015  Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
////////////////////////////////////////////////////////////////////////////////
// Name of the Module		: FTL interface / metadata processor
// Coded by
//                              : M S Santosh Kumar
//
// Module Description		: This module contains the ftl interface for NVMe controller.
//
// Functionality at a glance	: 
//			                1. Provides ftl interface from NVMe controller
//			                2. Schedules the IO requests across channels
//			                3. Abstracts the ftl layer for NVMe controller
//                                         giving the flexibility of implementing ftl at host/device side
//
////////////////////////////////////////////////////////////////////////////////


`include "global_parameters"

package FtlProcessor;

import global_definitions::*;
import FIFO::*;
import FIFOF::*;
import SpecialFIFOs::*;
import BRAMFIFO::*;
import BRAM::*;
import ConfigReg::*;
import Vector::*;

interface Ifc_ftl_in;
   method Action _put_cmd(Ftl_cmd ftl_cmd);
   method bit put_cmd_busy();
   method Action _put_prp(UInt#(64) main_mem_addr);
   method bit put_prp_busy();
   method Action _put_metadata (UInt#(64) _metadata);
   method bit put_metadata_busy();
endinterface

interface Ifc_ftl_out;
   method ActionValue#(Nand_cmd) get_cmd();
   method ActionValue#(Ftl_prp_tag) get_prp_read();
   method ActionValue#(Ftl_prp_tag) get_prp_write();
endinterface

interface Ifc_ftl_processor;
   interface Ifc_ftl_in ifc_ftl_in;
   interface Vector#(`NO_CHANNELS,Ifc_ftl_out) ifc_ftl_out;
endinterface

module mkFtlProcessor(Ifc_ftl_processor);
   Wire#(bit) dwr_cmd_busy <- mkDWire(1'b1);
   Wire#(bit) dwr_request_busy <- mkDWire(1'b1);
   FIFOF#(Ftl_cmd) ff_ftl_cmd <- mkSizedFIFOF(5);
   FIFOF#(UInt#(64)) bff_ftl_req <- mkSizedBRAMFIFOF(69);
   FIFOF#(UInt#(64)) bff_ftl_metadata <- mkSizedBRAMFIFOF(69);
   // TODO TODO unnecessary bit usage
  Vector#(2, Reg#(Bit#(16))) rg_request_counter <- replicateM(mkReg(0));
   Vector#(`NO_CHANNELS,Wire#(Nand_cmd)) wr_send_ftl_cmd <- replicateM(mkWire());
   Vector#(`NO_CHANNELS,Wire#(Ftl_prp_tag)) wr_ftl_prp_tag_read <- replicateM(mkWire());
   Vector#(`NO_CHANNELS,Wire#(Ftl_prp_tag)) wr_ftl_prp_tag_write <- replicateM(
      mkWire());
   Wire#(Bool) wr_deq_ftl_cmd <- mkWire();
   Wire#(Bool) wr_deq_ftl_req <- mkWire();
   Wire#(Bool) wr_deq_ftl_metadata <- mkWire();
   Reg#(UInt#(64)) rg_lba <- mkReg(0);
   Reg#(Bit#(16)) rg_metadata_counter <- mkReg(0);
   Reg#(Bit#(16)) rg_local_metadata_counter <- mkReg(0);
  // Debug signal
  Reg#(Bit#(16)) rg_debug_ftl <- mkRegU();
  // bram for storing the lba and pba
   BRAM1Port#(UInt#(16), UInt#(112)) bram_lba_pba <- mkBRAM1Server(defaultValue); //##

   rule rl_send_ftl_wcmd (ff_ftl_cmd.first().opcode == WRITE_NAND && ff_ftl_cmd.first().hybrid == 0);
     
//   Bit#(64) lv_channel_no = extend(pack(bff_ftl_metadata.first())[63:22]);
		
      Bit#(64) lv_channel_no;
    
      `ifdef NO_CHANNELS_1
	lv_channel_no = 0;
      `else	
        lv_channel_no = (
	 pack(bff_ftl_metadata.first())[fromInteger(valueOf(TLog#(`NO_CHANNELS))) : 0]);//previously -1 was there with NO_CHANNELS
      `endif

      $display ("%d: FtlProcessor: written ftl metadata %b write cmd opcode: %d and ftl prp to out addr: %x to wire channel: %d", 
	 $stime(), bff_ftl_metadata.first(), ff_ftl_cmd.first().opcode, bff_ftl_req.first(), lv_channel_no);
      wr_send_ftl_cmd [lv_channel_no] <= Nand_cmd {
	 opcode     : ff_ftl_cmd.first().opcode,
	 length     : 0,
	 nand_pba   : bff_ftl_metadata.first()
	 };
      
      wr_ftl_prp_tag_write [lv_channel_no] <= Ftl_prp_tag {
	 main_mem_addr : bff_ftl_req.first(),
	 tag           : ff_ftl_cmd.first().tag
	 };
	 
   endrule : rl_send_ftl_wcmd

   rule rl_send_ftl_hybrid_wcmd (ff_ftl_cmd.first().opcode == WRITE_NAND && ff_ftl_cmd.first().hybrid == 1);

      Bit#(64) lv_channel_no;
    
      `ifdef NO_CHANNELS_1
lv_channel_no = 0;
      `else	
        lv_channel_no = (
	 pack(bff_ftl_metadata.first())[fromInteger(valueOf(TLog#(`NO_CHANNELS))) : 0]);//previously -1 was there with NO_CHANNELS
      `endif

      $display ("%d: FtlProcessor: written ftl metadata %b HYBRID write cmd opcode: %d and ftl prp to out addr: %x to wire channel: %d", 
	 $stime(), bff_ftl_metadata.first(), ff_ftl_cmd.first().opcode, bff_ftl_req.first(), lv_channel_no);
      wr_send_ftl_cmd [lv_channel_no] <= Nand_cmd {
	 opcode     : ff_ftl_cmd.first().opcode,
	 length     : 0,
	 nand_pba   : bff_ftl_metadata.first()
	 };
      
      wr_ftl_prp_tag_write [lv_channel_no] <= Ftl_prp_tag {
	 main_mem_addr : bff_ftl_req.first(),
	 tag           : ff_ftl_cmd.first().tag
	 };

      let lv_pba = unpack({pack(rg_lba)[63:16],pack(bff_ftl_metadata.first())});

       bram_lba_pba.portA.request.put(BRAMRequest{ 
         write                  : True,
         address                : unpack(pack(rg_lba)[15:0]),
         datain                 : lv_pba,
         responseOnWrite        : False
         });
    $display("%d: storing lba 0x%x and pba 0x%x in bram",$stime(), pack(rg_lba)[15:0], lv_pba);
       rg_lba <= rg_lba + 'h1000;

   endrule : rl_send_ftl_hybrid_wcmd
    
    rule rl_send_ftl_rcmd (ff_ftl_cmd.first().opcode == READ_NAND && ff_ftl_cmd.first().hybrid == 0);

//     Bit#(64) lv_channel_no = extend(pack(bff_ftl_metadata.first())[63:22]);
     
      Bit#(64) lv_channel_no;
    
      `ifdef NO_CHANNELS_1
	lv_channel_no = 0;
      `else	
        lv_channel_no = (
	 pack(bff_ftl_metadata.first())[fromInteger(valueOf(TLog#(`NO_CHANNELS))) : 0]);//previously -1 was there with NO_CHANNELS
      `endif
      
       $display ("%d: FtlProcessor: written ftl read cmd opcode: %d and ftl prp to out addr: %x to wire channel: %d", 
	 $stime(), ff_ftl_cmd.first().opcode, bff_ftl_req.first(), lv_channel_no);
       wr_send_ftl_cmd [lv_channel_no] <= Nand_cmd {
	 opcode     : ff_ftl_cmd.first().opcode,
	 length     : 0,
	 nand_pba   : bff_ftl_metadata.first()
	 };

       wr_ftl_prp_tag_read [lv_channel_no] <= Ftl_prp_tag {
	 main_mem_addr : bff_ftl_req.first(),
	 tag           : ff_ftl_cmd.first().tag
	 };
    
    rg_debug_ftl[7:4] <= ff_ftl_cmd.first().tag;   
    endrule : rl_send_ftl_rcmd
// Hybrid Read
    rule rl_send_ftl_hybrid_bram_request_rcmd (ff_ftl_cmd.first().opcode == READ_NAND && ff_ftl_cmd.first().hybrid == 1);

          bram_lba_pba.portA.request.put(BRAMRequest{
          write			: False,
          address		: unpack(pack(rg_lba)[15:0]),
          datain		: ?,
          responseOnWrite	: False
          });
          $display("%d: requesting pba from bram lba address 0x%x ",$stime(),pack(rg_lba)[15:0]); 
	   rg_lba <= rg_lba + 'h1000;

    endrule: rl_send_ftl_hybrid_bram_request_rcmd
  
    rule rl_send_ftl_hybrid_rcmd (ff_ftl_cmd.first().opcode == READ_NAND && ff_ftl_cmd.first().hybrid == 1);

      let lv_data <- bram_lba_pba.portA.response.get();

      Bit#(64) lv_channel_no;

      `ifdef NO_CHANNELS_1
	lv_channel_no = 0;
      `else	
        lv_channel_no = (
	 pack(lv_data)[fromInteger(valueOf(TLog#(`NO_CHANNELS))) : 0]);//previously -1 was there with NO_CHANNELS
      `endif
      
       $display ("%d: FtlProcessor: written ftl HYBRID read cmd opcode: %d and ftl prp to out addr: %x to wire channel: %d nand_physical_address 0x%x", 
	 $stime(), ff_ftl_cmd.first().opcode, bff_ftl_req.first(), lv_channel_no, pack(lv_data)[63:0]);

       wr_send_ftl_cmd [lv_channel_no] <= Nand_cmd {
	opcode      : ff_ftl_cmd.first().opcode,
	length	    : 0,
	nand_pba    : unpack(pack(lv_data)[63:0])
	};
 
       wr_ftl_prp_tag_read [lv_channel_no] <= Ftl_prp_tag {
	 main_mem_addr : bff_ftl_req.first(),
	 tag           : ff_ftl_cmd.first().tag
	 };

     endrule : rl_send_ftl_hybrid_rcmd
// ppa read and write
   rule rl_send_ftl_ppa_wcmd (ff_ftl_cmd.first().opcode == WRITE_NAND && ff_ftl_cmd.first().hybrid == 2'b10);

      Bit#(64) lv_channel_no;
    
      `ifdef NO_CHANNELS_1
	lv_channel_no = 0;
      `else	
        lv_channel_no = (
	 pack(bff_ftl_metadata.first())[fromInteger(valueOf(TLog#(`NO_CHANNELS))) : 0]);//previously -1 was there with NO_CHANNELS
      `endif

      $display ("%d: FtlProcessor: written ftl metadata %b PPA write cmd opcode: %d and ftl prp to out addr: %x to wire channel: %d", 
	 $stime(), bff_ftl_metadata.first(), ff_ftl_cmd.first().opcode, bff_ftl_req.first(), lv_channel_no);
      wr_send_ftl_cmd [lv_channel_no] <= Nand_cmd {
	 opcode     : ff_ftl_cmd.first().opcode,
	 length     : 0,
	 nand_pba   : bff_ftl_metadata.first()
	 };
      
      wr_ftl_prp_tag_write [lv_channel_no] <= Ftl_prp_tag {
	 main_mem_addr : bff_ftl_req.first(),
	 tag           : ff_ftl_cmd.first().tag
	 };

   endrule : rl_send_ftl_ppa_wcmd
   
    rule rl_send_ftl_ppa_rcmd (ff_ftl_cmd.first().opcode == READ_NAND && ff_ftl_cmd.first().hybrid == 2'b10);

      Bit#(64) lv_channel_no;

      `ifdef NO_CHANNELS_1
	lv_channel_no = 0;
      `else	
        lv_channel_no = (
	 pack(bff_ftl_metadata.first())[fromInteger(valueOf(TLog#(`NO_CHANNELS))) : 0]);//previously -1 was there with NO_CHANNELS
      `endif
      
       $display ("%d: FtlProcessor: written ftl PPA read cmd opcode: %d and ftl prp to out addr: %x to wire channel: %d nand_physical_address 0x%x", 
	 $stime(), ff_ftl_cmd.first().opcode, bff_ftl_req.first(), lv_channel_no, bff_ftl_metadata.first());

       wr_send_ftl_cmd [lv_channel_no] <= Nand_cmd {
	opcode      : ff_ftl_cmd.first().opcode,
	length	    : 0,
	nand_pba    : bff_ftl_metadata.first()
	};
 
       wr_ftl_prp_tag_read [lv_channel_no] <= Ftl_prp_tag {
	 main_mem_addr : bff_ftl_req.first(),
	 tag           : ff_ftl_cmd.first().tag
	 };

     endrule : rl_send_ftl_ppa_rcmd

   rule rl_send_ftl_erase_cmd (ff_ftl_cmd.first().opcode == ERASE_NAND);

      Bit#(64) lv_channel_no;

      `ifdef NO_CHANNELS_1
	lv_channel_no = 0;
      `else	
        lv_channel_no = (
	 pack(bff_ftl_metadata.first())[fromInteger(valueOf(TLog#(`NO_CHANNELS))) : 0]);//previously -1 was there with NO_CHANNELS
      `endif

      wr_send_ftl_cmd [lv_channel_no] <= Nand_cmd {
	 opcode		: ff_ftl_cmd.first().opcode,
	 length		: 0,
	 nand_pba	: bff_ftl_req.first()
	 };
      
      wr_ftl_prp_tag_write [lv_channel_no] <= Ftl_prp_tag {
	 main_mem_addr	: ?,
	 tag		: ff_ftl_cmd.first().tag
	 };
      
   endrule : rl_send_ftl_erase_cmd    
   
   rule rl_deq_ftl_cmd (wr_deq_ftl_cmd);
      ff_ftl_cmd.deq();
   endrule : rl_deq_ftl_cmd
   
   rule rl_deq_ftl_req (wr_deq_ftl_req);
      bff_ftl_req.deq();
   endrule : rl_deq_ftl_req
   
   rule rl_deq_ftl_metadata (wr_deq_ftl_metadata);
      bff_ftl_metadata.deq();
   endrule : rl_deq_ftl_metadata
      
   Vector#(`NO_CHANNELS, Ifc_ftl_out) arr_ftl_out_ifc;
   for (Integer i = 0; i < `NO_CHANNELS; i = i + 1) begin
      arr_ftl_out_ifc[i] =
      (
       interface Ifc_ftl_out;
       
	  // The current design requires that get_map and get_cmd to be called simultaneously otherwise it will not work.
   
	  method ActionValue#(Nand_cmd) get_cmd();
	     $display ("%d: FtlProcessor: Sent cmd to nvme channel: %d opcode: %x",$stime(),i,wr_send_ftl_cmd[i].opcode);
	     return wr_send_ftl_cmd[i];
	  endmethod
   
	  method ActionValue#(Ftl_prp_tag) get_prp_read();

	     if (rg_request_counter[0] == ff_ftl_cmd.first().length)
		begin
         rg_request_counter[0] <= 0;
		   wr_deq_ftl_cmd <= True;
       end
	     else
		begin
		   rg_request_counter[0] <= rg_request_counter[0] + 1;
		end
       
	     wr_deq_ftl_metadata <= True;
	     wr_deq_ftl_req <= True;
	       
	     $display ("%d: FtlProcessor: Sent prp and tag to nvme channel: %d main_mem_addr: %x",$stime(),i,wr_ftl_prp_tag_read[i].main_mem_addr);
	     return wr_ftl_prp_tag_read[i];
      
	  endmethod
       
       method ActionValue#(Ftl_prp_tag) get_prp_write();

	     if (rg_request_counter[1] == ff_ftl_cmd.first().length)
		begin
		  rg_request_counter[1] <= 0;
		   wr_deq_ftl_cmd <= True;
		end
	     else
		begin
		   rg_request_counter[1] <= rg_request_counter[1] + 1;
		end
       
	     wr_deq_ftl_metadata <= True;
	     wr_deq_ftl_req <= True;
	       
	     $display ("%d: FtlProcessor: Sent prp and tag to nvme channel: %d main_mem_addr: %x",$stime(),i,wr_ftl_prp_tag_write[i].main_mem_addr);
	     return wr_ftl_prp_tag_write[i];
      
	  endmethod
   
       endinterface
       );
   end   
   
   interface Ifc_ftl_in ifc_ftl_in;
      method Action _put_cmd (Ftl_cmd ftl_cmd);
	 $display ("%d: FTLProcessor : enqueued ftl command",$stime());
	 ff_ftl_cmd.enq(ftl_cmd);
     rg_lba <= ftl_cmd.nand_lba;
      endmethod
      
      method bit put_cmd_busy();
	 return pack(!ff_ftl_cmd.notFull());
      endmethod
      
      method Action _put_prp (UInt#(64) _main_mem_addr);
	 if(ff_ftl_cmd.first().hybrid == 1 && ff_ftl_cmd.first().opcode != READ_NAND) begin
	  $display("%d: FTLProcessor:1 enqueued ftl request",$stime());
	  bff_ftl_req.enq(_main_mem_addr);
	 end
	 
	 else if(ff_ftl_cmd.first().hybrid == 0) begin
	  $display("%d: FTLProcessor:2 enqueued ftl request",$stime());
	  bff_ftl_req.enq(_main_mem_addr);
	 end
	 
	 else if(ff_ftl_cmd.first().hybrid == 2'b10) begin
	  $display("%d: FTLProcessor:3 enqueued ftl request",$stime());
	  bff_ftl_req.enq(_main_mem_addr);
	 end
	 
	 else if(ff_ftl_cmd.first().hybrid == 1 && ff_ftl_cmd.first().opcode == READ_NAND) begin
	  bff_ftl_req.enq(_main_mem_addr);
	 $display("%d: FTLProcessor: enqueued ftl request",$stime());
	 end
      endmethod
      
      method bit put_prp_busy();
	 return pack(!bff_ftl_req.notFull());
      endmethod
   
      method Action _put_metadata (UInt#(64) _metadata);
       if(ff_ftl_cmd.first().hybrid == 0 ) begin
	 $display("%d: FTLProcessor: enqueued ftl metadata",$stime());
	 bff_ftl_metadata.enq(_metadata);
       end

	else if(ff_ftl_cmd.first().hybrid == 1 && ff_ftl_cmd.first().opcode != READ_NAND) begin
	 $display("%d: FTLProcessor: enqueued ftl metadata HYBRID",$stime());
	 bff_ftl_metadata.enq(_metadata);
	end
	
	else if(ff_ftl_cmd.first().hybrid == 2'b10) begin
	 $display("%d: FTLProcessor: enqueued ftl metadata PPA",$stime());
	 bff_ftl_metadata.enq(_metadata);
	end

      endmethod
   
      method bit put_metadata_busy ();
	 return pack(!bff_ftl_metadata.notFull());
      endmethod
   
   endinterface
   
   interface ifc_ftl_out = arr_ftl_out_ifc;

endmodule

endpackage
