/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2013,2014  Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

////////////////////////////////////////////////////////////////////////////////
// Name of the Module	: NVM Express Wrapper/ PCI Express Controller
// Coded by		: M S Santosh Kumar 
//				: Vishvesh Sundararaman (MSI interrupt)
// Email Id             : ee10b017@ee.iitm.ac.in
// Module Description	: This module provides pci express interface for NVM Express module
//
//Functionality at a glance :
//     This module is compatible with xilinx 7 series integrated pci express core and provides
//     a wrapper for NVM express core to interface with PCI express.
//  
//     1. Detecting PCI express TLP and transmitting it to corresponding NVM Express interface
//     2. Conflict resolution for simultaneous request of PCI Express interface
//     3. Convert NVM express requests into PCI express TLP requests
//								
// References	        : pg054 xilinx 7 series pcie 
////////////////////////////////////////////////////////////////////////////////
`include "global_parameters"

package NvmWrapper;

`define MRC_FIFO_SIZE 5

import SpecialFIFOs::*;
import FIFO::*;
import FIFOF::*;
import NvmWrapperHeader::*;
import NvmController::*;
import DReg::*;
import ConfigReg::*;
import Vector::*;

//import NvmController_model::*;
import global_definitions::*;
//import global_defs_tb::*;
import FtlProcessor::*;

interface Ifc_nvme_wrapper;
   interface Ifc_tx_packet ifc_tx_packet;
   interface Ifc_rx_packet ifc_rx_packet;
   interface Ifc_config_space ifc_config_space;
   interface Vector#(`NO_CHANNELS,Ifc_nand_flash) ifc_nand_flash;
endinterface


/*-----States related to receive state machine-----*/
typedef enum {
   DECIDE_INTERFACE,
   CONFIG_WRITE,
   CONFIG_READ,
   COMPLETION,
   INVALID_PACKET   //This state is not used yet
   }WrapperStatesRx deriving(Bits,Eq);

/*-----States related to transmit state machine------*/
typedef enum {
   BEGIN_MRC_DTS,  //Begin Memory Read Completion or Data Transfer
   MRC_TRANSFER,
   INTERRUPT_TRANSFER,
   DATA_TRANSFER
   }WrapperStatesTx deriving(Bits,Eq);

/*------First DWord Header for PCIe TLP -----*/
typedef struct {
   Bit#(2) format;
   Bit#(5) packetType;
   Bit#(3) trafficClass;
   Bit#(1) tlpDigestAssert;
   Bool errorPoison;
   Bit#(2) attribute;
   Bit#(10) length;
   }HeaderDW0 deriving(Bits,Eq);

/*-----This struct holds the details of a configuration read packet-----*/
typedef struct {
   Bit#(16) requesterID;
   Bit#(8) tag;
   Bit#(32) address;
   bit dword_aligned;
   }ConfigReadPacket deriving(Bits,Eq);

module mkNvmWithWrapper (Ifc_nvme_wrapper);

   //-----------------Instantiating NVMe module------------------------//
   
   Ifc_Controller lv_nvm_controller <- mkNvmController;
   // Ifc_Controller_Rx_Tb lv_nvm_controller <- mkNvmController_model;
   Ifc_ftl_processor lv_ftl_proc <- mkFtlProcessor;
   
   /* renamings for better readability */

   let lv_nvm_nand_flash_ifc = lv_nvm_controller.ifc_nand_flash;
   let lv_nvme_ftl_proc_in = lv_nvm_controller.ifc_ftl_processor_in;
   let lv_nvme_ftl_proc_out = lv_nvm_controller.ifc_ftl_processor_out;
   let lv_ftl_in = lv_ftl_proc.ifc_ftl_in;
   let lv_ftl_out = lv_ftl_proc.ifc_ftl_out;

   
   //-----------------redefining for simplicity--------------------------------//
   
   // Currently the the pci express is connected to NvmController_model.
   // If it needs to be connected to actual NvmController
   // change lv_nvm_controller_rx_tb to lv_nvm_controller in the following lines.
   
   let lv_nvm_pcie_tx_ifc = lv_nvm_controller.nvmTransmitToPCIe_interface;
   let lv_nvm_config_ifc = lv_nvm_controller.ifc_config;
   let lv_nvm_compl_ifc = lv_nvm_controller.ifc_completion;
   let lv_nvm_interrupt_ifc = lv_nvm_controller.nvmInterruptSideA_interface; // interface for interrupt
   let lv_nvm_debug_ifc = lv_nvm_controller.ifc_debug; 

   /*-------------------Reg and Wire definitions: start----------------------*/

   //------------------Common registers and wires----------------------------//
   
   //These are used to count the number of pending mrc requests
   Reg#(UInt#(TLog#(`MRC_FIFO_SIZE))) rg_mrc_fifo_counter <- mkReg(0); //stores the received host memory read request
   Wire#(Bool) dwr_mrc_fifo_enq <- mkDWire(False); //asserted when the mrc fifo is enqueued
   Wire#(Bool) dwr_mrc_fifo_deq <- mkDWire(False); //asserted when the mrc fifo is dequeued
   Wire#(bit) dwr_test <- mkDWire(1'b0); //unused wire 

   //Register that holds receive state machine states
   Reg#(WrapperStatesRx) rg_wrapper_state_rx <- mkReg(DECIDE_INTERFACE);
   //Register that holds transmit state machine states
   Reg#(WrapperStatesTx) rg_wrapper_state_tx <- mkReg(BEGIN_MRC_DTS);

   //wires related to Requester ID
   Wire#(Bit#(8)) wr_bus_number <- mkWire(); //bus number given by pcie core
   Wire#(Bit#(5)) wr_device_number <- mkWire(); //device number given by pcie core
   Wire#(Bit#(3)) wr_function_number <- mkWire(); //function number given by pcie core


   //wires related to receive interface
   // look at pg054 xilinx 7 series integrated pcie documentation for more details
   Wire#(Bit#(128)) wr_rx_data_in <- mkWire();  //data in from pcie core
   Wire#(Bool) wr_rx_tvalid <- mkDWire(False);  // indicates the data in is valid
   Wire#(Bit#(7)) wr_bar_hit <- mkWire(); // unused
   Wire#(Bit#(4)) wr_rx_new_byte_loc <- mkWire(); //indicates the new byte location
   Wire#(Bool) wr_rx_new_packet_assert <- mkWire(); //indicates the presence of new packet
   Wire#(Bit#(4)) wr_rx_end_byte_loc <- mkWire(); //indicates the end byte location
   Wire#(Bool) wr_rx_end_packet_assert <- mkWire(); //indicates the end of packet
   Wire#(bit) wr_rx_err_fwd <- mkWire(); // unused
   Wire#(bit) wr_rx_ecrc_err <- mkWire(); // unused
   Wire#(bit) wr_rx_ready <- mkDWire(1); // indicates the pcie controller is ready
   Wire#(bit) wr_rx_np_ok <- mkDWire(1); // unused
   Wire#(bit) wr_rx_np_req <- mkDWire(1); // unused


   //Wires related to transfer interface
   // look at pg054 xilinx 7 series integrated pcie documentation for more details
   Reg#(Bit#(128)) rg_tx_data_out <- mkReg(0); //data out from pcie controller
   Reg#(Bool) rg_tx_end_packet_assert <- mkReg(False); //indicates end packet
   Reg#(Bit#(16)) rg_tx_valid_bytes <- mkReg(16'hFFFF); //indicates valid bytes in data out
   Reg#(Bool) rg_tx_valid <- mkReg(False); //indicates that the data out is valid
   Wire#(Bool) wr_pcie_ready <- mkWire();  //indicates that pcie core is ready
   Reg#(Bool) drg_tx_out_src_dsc <- mkDReg(False); //unused
   Wire#(Bit#(6)) wr_tx_buf_av <- mkWire(); //unused
   Wire#(Bool) wr_tx_err_drop <- mkWire(); //unused
   Reg#(Bool) rg_tx_conseq_packet_stream <- mkDReg(True); //indicates consequitive packet transfer
   Wire#(Bool) wr_tx_in_cfg_req <- mkWire(); //unused
   Wire#(Bool) wr_tx_in_cfg_gnt <- mkDWire(True); //unused
   Wire#(Bool) wr_tx_packet_error_poison <- mkDWire(False); //unused
   Wire#(Bool) wr_tx_ecrc_gen <- mkDWire(False); //unused
   Reg#(Bool) rg_tx_32_address <- mkReg(True);	//indicates 32 bit DTS addressing
   // Register for transmittind interrupt to pci
   Reg#(bit) rg_rx_msienable <- mkReg(0);
   Reg#(bit) rg_rx_cfg_intrpt_rdyN <- mkReg(0);
   Reg#(Bit#(8)) rg_tx_cfg_interruptDI <- mkRegU();
   Reg#(Bit#(3)) rg_rx_cfg_intrpt_mmenable <- mkReg(0);
   Reg#(bit) rg_tx_cfg_intrptN <- mkReg(0); 
    
   /*------------Registers related to receive state machine--------------------*/
   
   //These registers are used to pass information from one cycle to next cycle.
   Reg#(Bit#(32)) rg_pass_info1_32 <- mkReg(0);
   Reg#(Bit#(32)) rg_pass_info2_32 <- mkReg(0);
   //Indicates the presence of straddled data. ( see pg054 7 series pcie )
   Reg#(bit) rg_data_straddled <- mkReg(0);
   //This register is used to store the length of the received completion
   Reg#(Bit#(10)) rg_rx_compl_length <- mkReg(0);
   //This register store the completion tag
   Reg#(Bit#(8)) rg_rx_compl_tag <- mkReg(0);
   //This register stores 96bit payload data received during completion
   Reg#(Bit#(96)) rg_rx_data96 <- mkReg(0);
   //This register is used as a flag in COMPLETION state
   Reg#(Bool) rg_begin_cmpl <- mkReg(False);
   //This register is used to flag 32 bit or 64 bit access
   Reg#(bit) rg_write_dword_aligned <- mkReg(0);
   Reg#(bit) rg_read_dword_aligned <- mkReg(0);

  /*--------------------------------------------------------------------------*/

  /*----------------Registers related to transmit state machine---------------*/
   
   //This register holds data that needs to be processed in later clock cycles
   Reg#(Bit#(128)) rg_tx_data128 <- mkReg(0);
   //This register holds 32bit data that needs to be processed in later cycles
   Reg#(Bit#(32)) rg_tx_data32 <- mkReg(0);
   //This register holds the data transfer length that is written in 
   //BEGIN_MRC_DTS states and used in DATA_TRANFER state
   Reg#(Bit#(11)) rg_dts_length <- mkReg(0);
   //pipeline register for timing issue in vivado synthesis
  // Reg#(Bit#(11)) rg_dts_length_pip <- mkReg(0);

   // holds max payload size of the pcie device control register
   Reg#(Bit#(11)) rg_max_payload_size <- mkReg(11'h40);
   Reg#(Bit#(11)) rg_max_payload_counter <- mkReg(11'h40);
   // This register indicates that the split write is in progress
   Reg#(Bool) rg_dts_in_progress <- mkReg(False);
   // This register holds the split write address
   Reg#(Bit#(64)) rg_split_dts_address <- mkReg(0);

   /*--------------------------------------------------------------------------*/

   // FIFO for configuration read packet processing
   // Changed fifo from bypassfifo to normal fifo to meet timing
   FIFO#(ConfigReadPacket) ff_config_read_packet <- mkSizedFIFO(`MRC_FIFO_SIZE); 
   FIFOF#(Bit#(5)) ff_interrupt_vectr <- mkSizedFIFOF(`MRC_FIFO_SIZE);
   FIFOF#(bit) ff_interrupt <- mkSizedFIFOF(`MRC_FIFO_SIZE);

   /*----------------------reg and wire declarations: end---------------------------*/
   
   
   /*----------------------function declarations: start-----------------------------*/
   
   //This function converts a DWord from big to little endian format and viceversa
   function Bit#(32) big2littleEndian (Bit#(32) data);
      return { data[7:0], data[15:8], data[23:16], data[31:24] };
   endfunction
   
   //This function decides the target NVMe interface based on the format and packet type
   function WrapperStatesRx fn_target_interface(HeaderDW0 lv_header_dw0);
   
      if (lv_header_dw0.errorPoison) return INVALID_PACKET;
      else if (lv_header_dw0.format == 2'b10 && 
	       lv_header_dw0.packetType == 5'b0 )
	 return CONFIG_WRITE;
      else if	(lv_header_dw0.format == 2'b00 && 
	 lv_header_dw0.packetType == 5'b0 )
	 return CONFIG_READ;
      else if	(lv_header_dw0.format == 2'b10 && 
	 lv_header_dw0.packetType == 5'b1010 )
	 return COMPLETION;
      else return INVALID_PACKET;

   endfunction

   // This function handles data straddling at the end of every TLP processing
   // See pg054 xilinx 7 series pcie documentation pg no. 82 for details
   function Action handle_data_straddling(Bit#(128) data_in) = 
      action
	 HeaderDW0 lv_header_dw0;
	 lv_header_dw0 = HeaderDW0 {
	    format		: unpack(wr_rx_data_in[94:93]),
	    packetType		: unpack(wr_rx_data_in[92:88]),
	    trafficClass	: unpack(wr_rx_data_in[86:84]),
	    tlpDigestAssert	: unpack(wr_rx_data_in[79]),
	    errorPoison		: unpack(wr_rx_data_in[78]),
	    attribute		: unpack(wr_rx_data_in[77:76]),
	    length		: unpack(wr_rx_data_in[73:64])
	    };

	 rg_data_straddled <= 1;
	 let target_state = fn_target_interface(lv_header_dw0);
	 case(target_state)
	    CONFIG_WRITE:
	    begin
	       if (lv_header_dw0.length == 10'h1)
		  rg_write_dword_aligned <= 1'b1; //implies 32 bit access
	       else
		  rg_write_dword_aligned <= 1'b0; //implies 64 bit access
	       rg_wrapper_state_rx <= CONFIG_WRITE;
	    end
	    CONFIG_READ:
	    begin
	       if (lv_header_dw0.length == 10'h1)
		  rg_read_dword_aligned <= 1'b1;
	       else
		  rg_read_dword_aligned <= 1'b0;
	       
	       //wr_rx_data_in[119:112] corresponds to the transaction tag
	       //wr_rx_data_in[111:96] corresponds to requester ID
	       //stored in rg_pass_info1_32 which will be used in CONFIG_READ state
	       rg_pass_info1_32 <= {8'b0,wr_rx_data_in[119:112],
				    wr_rx_data_in[111:96]};
	       rg_wrapper_state_rx <= CONFIG_READ;
	    end
	    COMPLETION:
	    begin
	       rg_rx_compl_length <= wr_rx_data_in[73:64];
	       rg_wrapper_state_rx <= COMPLETION;
	       rg_begin_cmpl <= True;
	    end
	 endcase
      endaction;
   
   //This function should not be called unless PCIe ready is explicity
   //checked for.. Transfering MRC header requires PCIe ready to be asserted.
   //This is ensured in the rules that call this function.
   function ActionValue#(Bit#(10)) transfer_mrc_header() =
   actionvalue
      $display("%d: inside transfer_mrc_header",$stime());
      Bit#(2) lv_format;
      Bit#(5) lv_packet_type;
      Bit#(10) lv_length;
      Bit#(32) lv_header0;
      Bit#(32) lv_header1;
      Bit#(32) lv_header2;
      Bit#(16) lv_completerID;
      Bit#(16) lv_requesterID;
      Bit#(7) lv_lower_address;
      Bit#(3) lv_compl_status;
      Bit#(12) lv_byte_count;
      Bit#(8) lv_tag;
      Bit#(32) lv_address;
      Bit#(64) lv_data;
      ConfigReadPacket lv_config_read_packet;
      
      lv_format = 2'b10;
      lv_packet_type = 5'b01010;
      lv_completerID = {wr_bus_number,wr_device_number,wr_function_number};
      lv_byte_count = 12'h08;  //length*4
      lv_compl_status = 3'b0;
      //This rule using this implicitly fires only if the fifo is not empty
      lv_config_read_packet = ff_config_read_packet.first();
      // If the read packet is dword aligned, it implies a 32 bit access
      if (lv_config_read_packet.dword_aligned == 1'b1)
	 begin
	    lv_length = 10'h01;		//indicates 1DW data
	 end
      else
	 begin
	    lv_length = 10'h02;		//indicated 2DW data
	 end
      ff_config_read_packet.deq(); //deque the read packet
      dwr_mrc_fifo_deq <= True;
      lv_requesterID = lv_config_read_packet.requesterID;
      lv_tag = lv_config_read_packet.tag;
      lv_address = lv_config_read_packet.address;
      lv_lower_address = lv_address[6:0];
      
      //packet headers
      lv_header0 = {
	 1'b0,lv_format,lv_packet_type,	//Header0 Byte 3
	 8'b0,				//Header0 Byte 2
	 6'b0,lv_length[9:8],		//Header0 Byte 1
	 lv_length[7:0]		        //Header0 Byte 0
	 };
      lv_header1 = {
	 lv_completerID[15:8],				//Header1 Byte 3
	 lv_completerID[7:0],				//Header1 Byte 2
	 lv_compl_status,1'b0,lv_byte_count[11:8],	//Header1 Byte 1
	 lv_byte_count[7:0]				//Header1 Byte 0
	 };
      lv_header2 = {
	 lv_requesterID,	       //Header2 Byte 2 and 3. We send back
	                               //requesterID we get during request
	 lv_tag,		       //Header2 Byte 1
	 1'b0,lv_lower_address[6:0]    //Header2 Byte 0
	 };
      lv_data = lv_nvm_config_ifc.read_({15'h0,lv_address[16:0]});
      lv_data = {
	 big2littleEndian(lv_data[63:32]),
	 big2littleEndian(lv_data[31:0])
	 };
	    
      //This register holds the most significant DW that will be sent in the
      //next clock cycle
      if (lv_length == 10'h2)   //indicates 64 bit access and hence two DW data
	 rg_tx_data32 <= lv_data[63:32];

      rg_tx_data_out <= {lv_data[31:0],lv_header2,lv_header1,lv_header0};
      rg_tx_valid <= True;
      rg_tx_valid_bytes <= 16'hFFFF; //all bytes are valid
      if (lv_length == 10'h2)
	 rg_tx_end_packet_assert <= False;
      else
	 rg_tx_end_packet_assert <= True;
      return lv_length;

   endactionvalue;

   /*---------------------function declarations: end---------------------------*/
   
   /*---------------------rule declarations: start-----------------------------*/
   
   /* Make connection between ftl_processor and lv_nvm_to_ftl_ifc */
   
   rule rl_connect_ftl_cmd_in;
      let ftl_cmd_in = lv_nvme_ftl_proc_in.get_cmd();
      lv_ftl_in._put_cmd(ftl_cmd_in);
   endrule : rl_connect_ftl_cmd_in
   
   rule rl_connect_ftl_cmd_in_busy;
      let ftl_cmd_in_busy = lv_ftl_in.put_cmd_busy();
      lv_nvme_ftl_proc_in._get_cmd_busy(ftl_cmd_in_busy);
   endrule : rl_connect_ftl_cmd_in_busy

   rule rl_connect_ftl_request_in;
      let ftl_prp_in = lv_nvme_ftl_proc_in.get_prp();
      lv_ftl_in._put_prp(ftl_prp_in);
   endrule : rl_connect_ftl_request_in
   
   rule rl_connect_ftl_request_busy;
      let ftl_prp_in_busy = lv_ftl_in.put_prp_busy();
      lv_nvme_ftl_proc_in._get_prp_busy(ftl_prp_in_busy);
   endrule : rl_connect_ftl_request_busy
  
   rule rl_connect_ftl_metadata_in;
      let ftl_metadata_in = lv_nvme_ftl_proc_in.get_metadata();
      lv_ftl_in._put_metadata(ftl_metadata_in);
   endrule : rl_connect_ftl_metadata_in
   
   rule rl_connect_ftl_metadata_busy;
      let ftl_metadata_busy = lv_ftl_in.put_metadata_busy();
      lv_nvme_ftl_proc_in._get_metadata_busy(ftl_metadata_busy);
   endrule : rl_connect_ftl_metadata_busy
      
   Rules rls_connect_ftl_prp_out_read = emptyRules();
   Rules rls_connect_ftl_prp_out_write = emptyRules();
   
   for (Integer i = 0; i < `NO_CHANNELS; i = i + 1) begin      
      rule rl_connect_ftl_cmd_out;
	 let ftl_cmd_out <- lv_ftl_out[i].get_cmd();
	 lv_nvme_ftl_proc_out[i]._put_cmd(ftl_cmd_out);
	 $display ("%d: Tb_nvm: Enqueued command from ftl to nvm",$stime());
      endrule : rl_connect_ftl_cmd_out
      
      Rules rls_connect_ftl_prp_out_read_t = (rules
	 rule rl_connect_ftl_prp_out_read;
	    $display ("%d: Tb_nvm: Enqueued read prp from ftl to nvm",$stime());
	    let ftl_prp_out_read <- lv_ftl_out[i].get_prp_read();
	    lv_nvme_ftl_proc_out[i]._put_prp_read(ftl_prp_out_read);
	 endrule : rl_connect_ftl_prp_out_read
	 endrules);

      Rules rls_connect_ftl_prp_out_write_t = (rules
	 rule rl_connect_ftl_prp_out_write;
	    $display ("%d: Tb_nvm: Enqueued write prp from ftl to nvm",$stime());
	    let ftl_prp_out_write <- lv_ftl_out[i].get_prp_write();
	    lv_nvme_ftl_proc_out[i]._put_prp_write(ftl_prp_out_write);
	 endrule : rl_connect_ftl_prp_out_write
	 endrules);
      rls_connect_ftl_prp_out_read = rJoinConflictFree(
	 rls_connect_ftl_prp_out_read,rls_connect_ftl_prp_out_read_t);
      rls_connect_ftl_prp_out_write = rJoinConflictFree(
	 rls_connect_ftl_prp_out_write,rls_connect_ftl_prp_out_write_t);
   end
   
   Rules rls_connect_ftl_prp_out = rJoinConflictFree(rls_connect_ftl_prp_out_read,
      rls_connect_ftl_prp_out_write);

   addRules(rls_connect_ftl_prp_out);
      
   //This rule is used only for debugging purposes
   rule rl_debug;
	 
      $dumpvars();
      $display("%d: wrapper state 0x%x",$stime(),rg_wrapper_state_rx);
      $display("%d: transmit wrapper state 0x%x",$stime(),rg_wrapper_state_tx);
      $display("%d: nvme_data_valid: %d",$stime(),lv_nvm_pcie_tx_ifc.data_valid_());
      $display("%d: nvme_data_reg: 0x%x",$stime(),lv_nvm_pcie_tx_ifc.data_());
      $display("%d: wrapper_data_reg: 0x%x",$stime(),rg_tx_data128);
      $display("%d: pcie_data_reg: 0x%x pcie_data_valid: 0x%x",$stime(),rg_tx_data_out,
	 rg_tx_valid);
   endrule : rl_debug

   // memory read completion (mrc) counter 
   rule rl_mrc_fifo_counter;
   
      // Since the fifo is a bypass fifo, it is possible to enque and deque
      // in same clock cycle.
      // if the fifo is enqueued and not dequeued in the same cycle
      //   then the count is increased by 1
      // if the fifo is dequeued and not enqueued in the same cycle
      //   then the count is decreased by 1
      // if the fifo is enqueued and dequeued in the same cycle 
      //   the count remains the same.
      
      if (dwr_mrc_fifo_enq && !dwr_mrc_fifo_deq)
	 rg_mrc_fifo_counter <= rg_mrc_fifo_counter + 1;
      else if (!dwr_mrc_fifo_enq && dwr_mrc_fifo_deq)
	 rg_mrc_fifo_counter <= rg_mrc_fifo_counter - 1;
   
   endrule: rl_mrc_fifo_counter

   //////////////////////////////////////////////////////
   //  rules related to receive state machine: start   //
   //////////////////////////////////////////////////////

   //TODO TODO presence of TLP Digest is not handled
   
   //decides the nvme interface to which the packet data has to be sent
   //and sends it if possible in the present cycle itself.
   rule rl_decide_interface ((rg_wrapper_state_rx == DECIDE_INTERFACE) &&
			     wr_rx_new_packet_assert &&
			     wr_rx_tvalid );
	    
      $display("%d: rl_decide_interface fired data_in: 0x%x",$stime(),
	       wr_rx_data_in);
      HeaderDW0 lv_header_dw0;
      lv_header_dw0 = HeaderDW0 {
	 format			: wr_rx_data_in[30:29],
	 packetType   	        : wr_rx_data_in[28:24],
	 trafficClass   	: wr_rx_data_in[22:20],
	 tlpDigestAssert	: wr_rx_data_in[15],
	 errorPoison		: unpack(wr_rx_data_in[14]),
	 attribute		: wr_rx_data_in[13:12],
	 length			: wr_rx_data_in[9:0]
	 };
	    
      $display("format: %b packetType: %b errorPoison: %b",
	       lv_header_dw0.format,lv_header_dw0.packetType,
	       lv_header_dw0.errorPoison);
      
      let target_state = fn_target_interface(lv_header_dw0);
      $display("target_state: 0x%x",target_state);
      rg_data_straddled <= 0;
      case(target_state)
	 
	 // In this case we get Header0, Header1, Address, Data32 
	 // and hence storing Address and Data32 for next cycle
	 CONFIG_WRITE:
	 begin
	    Bit#(32) config_write_addr;
	    Bit#(32) config_write_data;
	   
	    config_write_addr = wr_rx_data_in[95:64];
	    $display("%d: mem_write packet received data_in: 0x%x. Going to next cycle to complete processing config_write_addr 0x%x data_part : 0x%x",
	       $stime(),wr_rx_data_in,config_write_addr,config_write_addr[3:0]);
	    
	    config_write_data = big2littleEndian(wr_rx_data_in[127:96]);
	    
	    if (lv_header_dw0.length == 10'h1)	  //implies 32 bit access
	       // 32 bit accesses can be finished in this cycle itself
	       begin
		  $display("%d: NvmWrapper: writing to address 0x%x, data 0x%x",
			   $stime(),config_write_addr[3:0],config_write_data);
		  lv_nvm_config_ifc._write(
		     {15'h0,config_write_addr[16:0]},
		     {32'h0,config_write_data}, True
		     );
		  
		  rg_wrapper_state_rx <= DECIDE_INTERFACE;
	       end
	    else if (lv_header_dw0.length == 10'h2) //implies 64 bit access
	       begin
		  //storing address and dataword0 for use in next cycle
		  rg_pass_info1_32 <= config_write_addr;
		  rg_pass_info2_32 <= config_write_data;
		  rg_wrapper_state_rx <= CONFIG_WRITE;
	       end
	    else
	       begin
		  rg_wrapper_state_rx <= DECIDE_INTERFACE;
	       end
	    
	 end
	 
	 // In this case we get Header0, Header1, Address
	 // and hence the the processing of this packet ends in this state.
	 CONFIG_READ:
	 begin
	    
	    $display("%d: mem_read packet received. Processing read packet.",
	       $stime());
	    ConfigReadPacket config_read_packet;
	    bit config_read_dword_aligned;
	    $display("%d: lv_header_dw0.length: %x",$stime(),lv_header_dw0.length);
	   
	    if (lv_header_dw0.length == 10'h1)
	       config_read_dword_aligned = 1'b1;
	    else
	       config_read_dword_aligned = 1'b0;
	    config_read_packet = ConfigReadPacket {
	       requesterID		: wr_rx_data_in[63:48],
	       tag			: wr_rx_data_in[47:40],
	       address			: wr_rx_data_in[95:64],
	       dword_aligned	        : config_read_dword_aligned
	       };
	    ff_config_read_packet.enq(config_read_packet);
	    dwr_mrc_fifo_enq <= True;
	    rg_wrapper_state_rx <= DECIDE_INTERFACE;
	    
	 end
	 
	 // TODO TODO compl.status if failed needs to be handled. currently packet
	 // is discarded
	 // TODO TODO check for valid completion payload has to be handled
	 // This is assuming the completion is valid !! what if the completion is
	 // not valid.. check for bytecount and required length to be added.
	 // This assumes that the completion payload is a multiple of 128 bit which is typical
	 // If the payload is not multiple of 128 bits, the behaviour is undefined
	 
	 COMPLETION:
	    
	 begin
	    $display("%d: completion received",$stime());
	    Bit#(3) lv_compl_status = wr_rx_data_in[47:45];
	    Bit#(10) lv_compl_length = wr_rx_data_in[9:0];
	    
	    if (lv_compl_status == 3'b000)
	       begin
		  if(lv_compl_length == 10'b0)   //implies 1024 DW payload
		     begin
			rg_rx_compl_length <= 10'h3FF;  //one DW is already received here
		     end
		  else
		     begin
			rg_rx_compl_length <= lv_compl_length - 1;
		     end
		  
		  //store the first DW received in this cycle. Will be sent to 
		  //NVMe in the next cycle
		  
		  rg_pass_info1_32 <= big2littleEndian(wr_rx_data_in[127:96]);
		  rg_wrapper_state_rx <= COMPLETION;
		  rg_rx_compl_tag <= wr_rx_data_in[79:72];
	       end
	 end
	 
	 // Handling of invalid packets is not yet supported.
	 // This state is only used for debugging.
	 
	 /// only for debugging ///
	 INVALID_PACKET:
	 begin
	 end
	 ///////////////////////
	 
      endcase
      
   endrule : rl_decide_interface
   
   //In this rule, we receive the remaining data payload of completion
   rule rl_completion (rg_wrapper_state_rx == COMPLETION && wr_rx_tvalid);
      
      Bool lv_not_last_completion_dw;
      
      if (rg_rx_compl_length > 10'h04)
	 lv_not_last_completion_dw = True;
      else
	 lv_not_last_completion_dw = False;
      
      if (lv_not_last_completion_dw)
	 begin
	    rg_wrapper_state_rx <= COMPLETION;
	 end
      else
	 begin
	    // assertion of new packet in this state implies the presence of
	    // straddled data
	    
	    if(wr_rx_new_packet_assert)
	       begin
		  $display("%d: data straddle handling ST: COMPLETION",$stime());
		  handle_data_straddling(wr_rx_data_in);
	       end
	    else
	       begin
		  rg_wrapper_state_rx <= DECIDE_INTERFACE;
	       end
	 end
      // rg_data_straddled is 1'b1 implies data is already straddled in the earlier state
      if (rg_data_straddled == 1'b1)
	 begin
	    
	    //rg_begin_cmpl is used to indicate the first cycle of completion packet
	    if(rg_begin_cmpl && !wr_rx_new_packet_assert)
	       begin
		  let lv_compl_tag = wr_rx_data_in[15:8];
		  rg_rx_compl_tag <= lv_compl_tag;
		  let lv_cmpl_data_in = wr_rx_data_in[127:32];
		  lv_cmpl_data_in = {
		     big2littleEndian(lv_cmpl_data_in[95:64]),
		     big2littleEndian(lv_cmpl_data_in[63:32]),
		     big2littleEndian(lv_cmpl_data_in[31:0])
		     };
		  rg_rx_data96 <= lv_cmpl_data_in;
		  if (rg_rx_compl_length == 10'b0)
			rg_rx_compl_length <= 10'h3FC;
		  else
			rg_rx_compl_length <= rg_rx_compl_length - 3;
		  if (!wr_rx_new_packet_assert)
		     rg_begin_cmpl <= False;
	       end
	    else 
	       //straddled data and not the start of completion packet
	       begin
		  lv_nvm_compl_ifc._write(
		     {big2littleEndian(wr_rx_data_in[31:0]),rg_rx_data96},
		     {8'b0,rg_rx_compl_tag});
		  if (lv_not_last_completion_dw)
		     begin
			let lv_cmpl_data_in = wr_rx_data_in[127:32];
			lv_cmpl_data_in = {
			   big2littleEndian(lv_cmpl_data_in[95:64]),
			   big2littleEndian(lv_cmpl_data_in[63:32]),
			   big2littleEndian(lv_cmpl_data_in[31:0])
			   };
			rg_rx_data96 	   <= lv_cmpl_data_in;
			rg_rx_compl_length <= rg_rx_compl_length - 4;
		     end
	       end
	 end
      else
	 begin
	    // not straddled data
	    let lv_cmpl_data_in = wr_rx_data_in[95:0];
	    lv_cmpl_data_in = {
	       big2littleEndian(lv_cmpl_data_in[95:64]),
	       big2littleEndian(lv_cmpl_data_in[63:32]),
	       big2littleEndian(lv_cmpl_data_in[31:0])
	       };
	    lv_nvm_compl_ifc._write(
	       {lv_cmpl_data_in,rg_pass_info1_32},
	       {8'b0,rg_rx_compl_tag});
	    if (lv_not_last_completion_dw && !wr_rx_new_packet_assert)
	       begin
		  let lv_cmpl_data_in_1 = wr_rx_data_in[127:96];
		  lv_cmpl_data_in_1 = big2littleEndian(lv_cmpl_data_in_1);
		  rg_pass_info1_32 <= lv_cmpl_data_in_1;
		  rg_rx_compl_length <= rg_rx_compl_length - 4;
	       end
	 end
   endrule : rl_completion
   
   //This rule completes the configuration write to NVMe
   rule rl_config_write ((rg_wrapper_state_rx == CONFIG_WRITE) && 
			 wr_rx_tvalid && wr_rx_end_packet_assert);
      
      $display("%d: rl_config_write fired", $stime());
      //TODO TODO Add valid length checking
      Bit#(64) config_data64;
      Bit#(32) config_address32;

      if(rg_data_straddled == 1)
	 begin
	    config_address32 = unpack(wr_rx_data_in[31:0]);
	    config_data64 = unpack(wr_rx_data_in[95:32]);
	    if (rg_write_dword_aligned == 1'b1) //implies 32 bit access
	       begin
		  lv_nvm_config_ifc._write(
		     {15'h0,config_address32[16:0]},
		     {32'h0,config_data64[31:0]},
		     True);
	       end
	    else
	       begin
		  lv_nvm_config_ifc._write(
		     {15'h0,config_address32[16:0]},
		     config_data64, False );
	       end
	    // There is a possibility of data straddle here in case it is a 32 bit access
	    if (wr_rx_new_packet_assert)
	       begin
		  handle_data_straddling(wr_rx_data_in);
	       end
	    else
	       begin
		  rg_wrapper_state_rx <= DECIDE_INTERFACE;
	       end
	    $display("%d: data straddled.ST: CONFIG_WRITE address: 0x%x data: 0x%x",
	       $stime(),config_address32,config_data64);
	 end
      
      // If data is not straddled, then we get only Data32 in this state and 
      // there is a possiblity of a new straddle. This also implies that the 
      // access is a 64-bit access
      else
	 begin
	    config_address32 = unpack(rg_pass_info1_32);
	    config_data64 = unpack({wr_rx_data_in[31:0],rg_pass_info2_32});
	    lv_nvm_config_ifc._write({15'h0,config_address32[16:0]},config_data64,True);
	  	    
	    $display("%d: data not straddled. ST: CONFIG_WRITE address: 0x%x data: 0x%x",$stime(),config_address32,config_data64);
	    
	    //data straddle handling
	    if(wr_rx_new_packet_assert)
	       begin
		  $display("%d: data straddle handling ST: CONFIG_WRITE",$stime());
		  handle_data_straddling(wr_rx_data_in);
	       end
	    else
	       begin
		  rg_wrapper_state_rx <= DECIDE_INTERFACE;
	       end
	 end		
   endrule : rl_config_write
   
   //we get to this rule only if the read packet is staddled.
   //There is a possibility of further straddling in this rule.
   rule rl_config_read ((rg_wrapper_state_rx == CONFIG_READ) && 
			wr_rx_tvalid && wr_rx_end_packet_assert);
      
      $display("%d: rl_config_read fired implying presence of straddled data",
	       $stime());
      ConfigReadPacket lv_config_read_packet;
      lv_config_read_packet = ConfigReadPacket {
	 requesterID		: unpack(rg_pass_info1_32[15:0]),
	 tag				: unpack(rg_pass_info1_32[23:16]),
	 address			: unpack(wr_rx_data_in[31:0]),
	 dword_aligned	: rg_read_dword_aligned
	 };
      ff_config_read_packet.enq(lv_config_read_packet);
      dwr_mrc_fifo_enq <= True;
      
      //data straddle handling
      if(wr_rx_new_packet_assert)
	 begin
	    $display("%d: data straddle handling ST: CONFIG_WRITE",$stime());
	    handle_data_straddling(wr_rx_data_in);
	 end
      else
	 begin
	    rg_wrapper_state_rx <= DECIDE_INTERFACE;
	 end
   endrule : rl_config_read
   
   /////////////////////////////////////////////////////
   //  rules related to receive state machine: end  ///
   /////////////////////////////////////////////////////
   
   /////////////////////////////////////////////////////
   //	rules related to transmit state machine: start //
   /////////////////////////////////////////////////////

   
   // This rule always asserts the wait to nvme as 0 when
   // the wrapper state is BEGIN_MRC_DTS. That rule which
   // requires the wait to be 1 always overrides this rule and hence
   // the descending urgency
   
   
   // If pcie is ready we need to invalidate the data that is present in the
   // pcie tramsmit registers for the next clock cycle since the data will
   // be processed in the present cycle. That rule that does otherwise always
   // overrides this rule and hence the descending urgency.
   
   (*descending_urgency = "rl_begin_mrc,rl_invalidate_data"*)
   rule rl_invalidate_data ( 	
      rg_wrapper_state_tx == BEGIN_MRC_DTS &&
      wr_pcie_ready &&
      lv_nvm_pcie_tx_ifc.data_valid_() != 1'b1);
      
      $display("%d: rl_invalidate_data fired",$stime());
      rg_tx_valid <= False;
      
   endrule : rl_invalidate_data
   
   // BEGIN_MRC_DTS state transfer pending memory read completions (mrc) if no data transfer is
   // requested from NVMe. If nvme requests a data transfer then data transfer is given 
   // priority but at the end of every data transfer all the pending mrc are processed.
   // If there is no pending mrc or data transfer, the state is idle.
   
   // configuration read is given more priority than data transfer
   (*descending_urgency="rl_begin_mrc,rl_begin_dts"*)
   
   // This rule fires if the nvme requests a data transfer
   rule rl_begin_dts ( rg_wrapper_state_tx == BEGIN_MRC_DTS &&
		      lv_nvm_pcie_tx_ifc.data_valid_() == 1'b1 && 
		      wr_pcie_ready);
	    
      $display("%d: rl_begin_dts fired",$stime());
      Bit#(2) lv_format;
      Bit#(5) lv_packet_type;
      Bit#(10) lv_length;
      Bit#(32) lv_header0;
      Bit#(16) lv_requesterID;
      Bit#(8) lv_tag;
      Bit#(4) lv_last_DW_BE;
      Bit#(4) lv_first_DW_BE;
      Bit#(32) lv_header1;
      Bit#(32) lv_address0;
      Bit#(32) lv_address1;
      Bit#(64) lv_address;
      Bit#(32) lv_length_nvme;
      bit lv_write;
      Bit#(128) lv_data_out;
      Bit#(128) lv_store_data;    //stores data that will be sent in next clk cycle
      WrapperStatesTx lv_next_state;
      Bool lv_end_packet_assert;
      Bit#(16) lv_valid_bytes;
      
      lv_store_data = lv_nvm_pcie_tx_ifc.data_();
      lv_store_data = {
	 big2littleEndian(lv_store_data[127:96]),
	 big2littleEndian(lv_store_data[95:64]),
	 big2littleEndian(lv_store_data[63:32]),
	 big2littleEndian(lv_store_data[31:0])
	 };
   
      // storing the data that will be sent in next clock cycle
      rg_tx_data128 <= lv_store_data;
      lv_write = lv_nvm_pcie_tx_ifc.write_();
      $display("%d: NvmWrapper lv_write: 0x%x",$stime(),lv_write);
      lv_tag = lv_nvm_pcie_tx_ifc.tag_()[7:0]; //tag is only 8 bit
      lv_length_nvme = lv_nvm_pcie_tx_ifc.payload_length_();
      $display("%d: NvmWrapper lv_length: 0x%x",$stime(),lv_length_nvme);
      
      // if it is a new data transfer use the address from the nvm controller
      // else if it a split data transfer, use the address stored during the
      // previous split
      
      if (rg_dts_in_progress)
	 begin
	    lv_address = rg_split_dts_address;
	    rg_split_dts_address <= rg_split_dts_address + 4*extend(rg_max_payload_size);
	 end
      else
	 begin
	    lv_address = pack(lv_nvm_pcie_tx_ifc.address_());
	    rg_split_dts_address <= lv_address + 4*extend(rg_max_payload_size);
	 end

      lv_address0 = lv_address[31:0];
      lv_address1 = lv_address[63:32];
      
      let lv_dts_length = rg_dts_in_progress ? rg_dts_length : lv_length_nvme[10:0];
      rg_dts_length <= lv_dts_length;
     // rg_dts_length <= rg_dts_length_pip;
     
      // if the data transfer length requested by nvme is more than the max payload size
      // the packet is split into a size of max payload size and the rest.
      if (lv_write == 1'b0)
	 // read request
	 lv_length = lv_length_nvme[9:0];
      else if (lv_dts_length > rg_max_payload_size)
	 lv_length = rg_max_payload_size[9:0];
      else
	 lv_length = lv_dts_length[9:0];
      
      // rg_dts_in_progress implies that split data transfer is yet to be completed.
      // The data transfer initiated by the nvme can be greater than max_payload_size
      // in which case the packet will be split and rg_dts_in_progress is asserted true
      // untill all the split packets are transfered.
      
      if (lv_write == 1'b1 && !rg_dts_in_progress)
	 rg_dts_in_progress <= True;
      
      $display("%d: NvmWrapper lv_length: 0x%x",$stime(),lv_length);
      if (lv_write == 1'b1)
	 begin
	    if (lv_address1 == 32'h0)
	       lv_format = 2'b10;
	    else
	       lv_format = 2'b11;
	 end
      else
	 begin
	    if (lv_address1 == 32'h0)
	       lv_format = 2'b00;
	    else
	       lv_format = 2'b01;
	 end
      lv_packet_type = 5'b00;
      lv_requesterID = {wr_bus_number,wr_device_number,wr_function_number};
     
      //Indicates all bytes in TLP are enabled
      lv_last_DW_BE = 4'b1111;			
      lv_first_DW_BE = 4'b1111;
      
      //Packet headers
      lv_header0 = {
	 1'b0,lv_format,lv_packet_type,  //Header0 Byte 3
	 8'b0,			         //Header0 Byte 2
	 6'b0,lv_length[9:8],	         //Header0 Byte 1
	 lv_length[7:0]		         //Header0 Byte 0
	 };
      lv_header1 = {
	 lv_requesterID[15:8],	         //Header1 Byte 3
	 lv_requesterID[7:0],	         //Header1 Byte 2
	 lv_tag,			 //Header1 Byte 1
	 lv_last_DW_BE,lv_first_DW_BE    //Header1 Byte 0
	 };
      if (lv_write == 1)  //memory write request
	 begin
	    $display ("%d: NvmWrapper: processing memory write request",$stime());
	    if(lv_address1 == 32'h0)
	       begin
		  lv_data_out = {lv_store_data[31:0],lv_address0,lv_header1,lv_header0};
		  rg_tx_32_address <= True;
	       end
	    else
	       begin
		  lv_data_out = {lv_address0,lv_address1,lv_header1,lv_header0};
		  rg_tx_32_address <= False;
	       end
	    lv_next_state = DATA_TRANSFER;
	    lv_end_packet_assert = False;
	    rg_max_payload_counter <= rg_max_payload_size;
	    lv_valid_bytes = 16'hFFFF;
	 end 
      else
	 begin
	    $display ("%d: NvmWrapper: processing memory read request",$stime());
	    //memory read request will be finished in this cycle itself
	    //no data to transfer
	    lv_next_state = BEGIN_MRC_DTS;
	    lv_end_packet_assert = True;
	    if (lv_address1 == 32'h0)
	       begin
		  lv_valid_bytes = 16'h0FFF;
		  lv_data_out = {32'h0,lv_address0,lv_header1,lv_header0};
	       end
	    else 
	       begin
		  lv_valid_bytes = 16'hFFFF;
		  lv_data_out = {lv_address0,lv_address1,lv_header1,lv_header0};
	       end
	 end
      
      rg_tx_valid <= True;
      lv_nvm_pcie_tx_ifc._wait(1'b0);
      rg_tx_data_out <= lv_data_out;
      rg_wrapper_state_tx <= lv_next_state;
      rg_tx_end_packet_assert <= lv_end_packet_assert;
      rg_tx_valid_bytes <= lv_valid_bytes;
      
   endrule
   
   // This rule sends configuration read completion
   // This rule fires only if the ff_config_read_packet FIFO is not empty
   // This rule will fire only if the pcie is ready

   rule rl_begin_mrc ( rg_wrapper_state_tx == BEGIN_MRC_DTS &&
		      wr_pcie_ready );
      
      Bit#(10) lv_length;
      $display("%d: rl_begin_mrc fired",$stime());
      lv_length <- transfer_mrc_header();
      $display("%d: lv_length: %x",$stime(),lv_length);
      lv_nvm_pcie_tx_ifc._wait(1'b1);
      
      if (lv_length == 10'h2)
	 begin
	    rg_wrapper_state_tx <= MRC_TRANSFER;
	 end
      else
	 begin
	    rg_wrapper_state_tx <= BEGIN_MRC_DTS;
	 end
            
   endrule : rl_begin_mrc
   
   // This rule finishes the memory read completion initiated in the
   // previous state
   rule rl_mrc_transfer_finish (rg_wrapper_state_tx == MRC_TRANSFER && wr_pcie_ready);
      
      $display("%d: rl_mrc_transfer_finish fired",$stime());
      //trasmitting the most significant DW that is stored in the
      //previous cycle
      rg_tx_data_out <= {96'b0,rg_tx_data32};
      rg_tx_valid_bytes <= 16'h000F; //only the last DW is valid 
      rg_tx_valid <= True;
      rg_tx_end_packet_assert <= True;
      rg_wrapper_state_tx <= BEGIN_MRC_DTS;
      lv_nvm_pcie_tx_ifc._wait(1'b1);
   endrule : rl_mrc_transfer_finish

//interrupt enqueue rule
rule rl_interrupt_enqueue;
 ff_interrupt_vectr.enq(lv_nvm_interrupt_ifc.vector_number());
 ff_interrupt.enq(lv_nvm_interrupt_ifc.vector_rdy());
endrule:rl_interrupt_enqueue
 
/* rule for interrupt transfer to the pcie xilinx core
   controller inserts interrupt and wait for msienable pin to be enabled.
   when msi pin gets enabled the msi vector value is send to the pcie core*/

   rule rl_interrupt_vector_transfer (wr_pcie_ready && rg_rx_msienable == 1);

    $display("%d: rl_interrupt_transfer vector number sent", $stime());
    let lv_vectr = ff_interrupt_vectr.first();
    rg_tx_cfg_interruptDI <= {3'b0, pack(lv_vectr)};
   // rg_tx_cfg_intrptN <= 0;
    ff_interrupt_vectr.deq();
   endrule :rl_interrupt_vector_transfer
  
   rule rl_interrupt_transfer(wr_pcie_ready && rg_rx_cfg_intrpt_rdyN == 0);
   
    rg_tx_cfg_intrptN <= ff_interrupt.first();
    ff_interrupt.deq();
   endrule: rl_interrupt_transfer

   rule rl_deasserting_interrupt(wr_pcie_ready && rg_rx_cfg_intrpt_rdyN == 1);
    rg_tx_cfg_intrptN <= 0;
   endrule: rl_deasserting_interrupt

   // If the pcie is not ready, we de-assert the pcie ready signal
   rule rl_wait_for_pcie (!wr_pcie_ready);
      
      lv_nvm_pcie_tx_ifc._wait(1'b1);
      $display("%d: rl_wait_for_pcie fired",$stime());
      
   endrule
 
   // The nvme data valid bit has to be one when we are in this state
   // The data transfer of NVMe has to be in continuous cycles.
   
   rule rl_data_transfer (rg_wrapper_state_tx == DATA_TRANSFER && wr_pcie_ready);
	 
      $display("%d: rl_data_tranfer fired",$stime());
      Bit#(128) lv_data_out;
      Bool lv_end_packet_assert;
      Bit#(16) lv_valid_bytes;
      Bit#(128) lv_store_data;
      Bool lv_last_data_transfer;
      
      if (rg_dts_length == 11'h04 || rg_max_payload_counter == 11'h04)
	 lv_last_data_transfer = True;
      else
	 lv_last_data_transfer = False;
            
      if (lv_last_data_transfer)
	 lv_nvm_pcie_tx_ifc._wait(1'b1);
      else
	 lv_nvm_pcie_tx_ifc._wait(1'b0);
            
      $display("%d: rg_dts_length: %d",$stime,rg_dts_length);
      
      //Next state logic
      if (lv_last_data_transfer)
	 begin
	    rg_wrapper_state_tx <= BEGIN_MRC_DTS;
	    lv_store_data = 0;
	 end
      else
	 begin
	    rg_wrapper_state_tx <= DATA_TRANSFER;
	    lv_store_data = lv_nvm_pcie_tx_ifc.data_();
	    lv_store_data = {
 	       big2littleEndian(lv_store_data[127:96]),
	       big2littleEndian(lv_store_data[95:64]),
	       big2littleEndian(lv_store_data[63:32]),
	       big2littleEndian(lv_store_data[31:0])
	       };    
	    rg_tx_data128 <= lv_store_data;
	 end
      
      rg_dts_length <= rg_dts_length - 4;
      rg_max_payload_counter <= rg_max_payload_counter - 4;
      
      
      // data transfer
      if (rg_tx_32_address)
	 lv_data_out = {lv_store_data[31:0],rg_tx_data128[127:32]};
      else
	 lv_data_out = rg_tx_data128;
      
      if (lv_last_data_transfer)
	 lv_end_packet_assert = True;
      else
	 lv_end_packet_assert = False;
      
      if (lv_last_data_transfer && rg_tx_32_address)
	 lv_valid_bytes = 16'h0FFF;
      else
	 lv_valid_bytes = 16'hFFFF;
      
      rg_tx_data_out <= lv_data_out;
      rg_tx_valid <= True;
      rg_tx_valid_bytes <= lv_valid_bytes;
      rg_tx_end_packet_assert <= lv_end_packet_assert;

      // transfer in progress set/unset
      if (rg_dts_length == 11'h04)
	 rg_dts_in_progress <= False;
      
   endrule
      
   /*-------------------------------------------------------------------*/
   
   /*-------------interface declarations: start-------------------------*/
   
   //configuration interface
   interface Ifc_config_space ifc_config_space;
      method Action _cfg_in_reqID ( Bit#(8) _bus_number,
				   Bit#(5) _device_number,
				   Bit#(3) _function_number );
      
	 wr_bus_number <= _bus_number;
	 wr_device_number <= _device_number;
	 wr_function_number <= _function_number;
      
   
      endmethod
   endinterface
   
   //receive interface
   interface Ifc_rx_packet ifc_rx_packet;
      method Action _rx_in (
	 Bit#(128) _tdata,
	 Bool _tvalid,
	 Bit#(7) _bar_hit 
	 );
	 wr_rx_data_in <= _tdata;
	 wr_rx_tvalid <= _tvalid;
	 wr_bar_hit <= _bar_hit;
      endmethod
      method Action _rx_in_tuser_sof ( 
	 Bool _new_packet_assert,
	 Bit#(4) _new_byte_loc
	 );
	 wr_rx_new_packet_assert <= _new_packet_assert;
	 wr_rx_new_byte_loc <= _new_byte_loc;
      endmethod
      method Action _rx_in_tuser_eof (
	 Bool _end_packet_assert,
	 Bit#(4) _end_byte_loc
	 );
	 wr_rx_end_packet_assert <= _end_packet_assert;
	 wr_rx_end_byte_loc <= _end_byte_loc;
      endmethod
      method Action _rx_in_tuser_err (
	 bit _fwd,
	 bit _ecrc_err);
	 wr_rx_err_fwd <= _fwd;
	 wr_rx_ecrc_err <= _ecrc_err;
      endmethod
      method Action _rx_msienable(bit _msienable);
	rg_rx_msienable <= _msienable;
      endmethod
      method Action _rx_cfg_intrpt_rdyN(bit _readyN);
	rg_rx_cfg_intrpt_rdyN <= _readyN;
      endmethod 
      method Action _rx_cfg_intrpt_mmenable(Bit#(3) _mmenable);
        rg_rx_cfg_intrpt_mmenable <= _mmenable;
      endmethod
      method bit rx_out_tready();
	 return wr_rx_ready;
      endmethod
      method bit rx_out_np_ok();
	 return wr_rx_np_ok;
      endmethod
      method bit rx_out_np_req();
	 return wr_rx_np_req;
      endmethod      
   endinterface
   
   //Transmit interface
   interface Ifc_tx_packet ifc_tx_packet;
      
      method Bit#(128) _tx_data_out ();
	 return rg_tx_data_out;
      endmethod
      method Bool _tx_out_eof_assert ();
	 return rg_tx_end_packet_assert;
      endmethod
      method Bit#(16) _tx_out_byte_enable ();
	 return rg_tx_valid_bytes;
      endmethod
      method Bool _tx_out_tvalid ();
	 return rg_tx_valid;
      endmethod
      method Action _tx_in_tready (Bool _pcie_ready);
	 wr_pcie_ready <= _pcie_ready;
      endmethod
      method Bool _tx_out_src_dsc ();
	 return drg_tx_out_src_dsc;
      endmethod
      method Action _tx_in_buf_av (Bit#(6) _tx_buf_av);
	 wr_tx_buf_av <= _tx_buf_av;
      endmethod
      method Action _tx_in_err_drop	(Bool _tx_err_drop);
	 wr_tx_err_drop <= _tx_err_drop;
      endmethod
      method Bool _tx_out_packet_stream ();
	 return rg_tx_conseq_packet_stream;
      endmethod
      method Action _tx_in_cfg_req (Bool _pcie_cfg_req);
	 wr_tx_in_cfg_req <= _pcie_cfg_req;
      endmethod
      method Bool _tx_in_cfg_gnt ();
	 return wr_tx_in_cfg_gnt;
      endmethod
      method Bit#(8) _tx_cfg_interruptDI();
       return rg_tx_cfg_interruptDI;
      endmethod
      method bit _tx_cfg_intrptN();
	  return rg_tx_cfg_intrptN;
      endmethod
      method Bool _tx_out_err_fwd ();
	 return wr_tx_packet_error_poison;
      endmethod
      method Bool _tx_out_ecrc_gen ();
	 return wr_tx_ecrc_gen;
      endmethod
      method Bit#(5) _tx_out_debug();
	 return lv_nvm_debug_ifc.debug();
      endmethod
     method Completion_status_type _tx_out_compl_status();
	 return lv_nvm_debug_ifc.debug_compl();
	 endmethod
     method Bit#(16) _tx_out_debug_info();
	 return lv_nvm_debug_ifc.debug_info();
	 endmethod
   endinterface

   interface ifc_nand_flash = lv_nvm_nand_flash_ifc;
endmodule
endpackage
