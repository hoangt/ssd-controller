
SSD HW / LightNVM Codesign
--------------------------
(Matias Bjorling, G. S. Madhusudan, Maximilian Singh)

* Compression and Dedup
 ** For the first iteration, this should be host-side.
  *** Having the host-side part complicates the design when we have to implement
      it on the device.
  *** Mainly, that we don't know how well something compresses / dedupes within
      the device.  Thereby we can't know what place data are written at IO
      submission time (from the host).
 ** For the second iteration, this can be fixed with a "nameless writes"
    approach.  The device lets us know after a write (in the completion event),
    where the data has been written.

* Encryption
 ** Encryption shall be implemented in HW.
  *** If the device is shared across servers, this is easier to manager.
  *** Later, a distributed metadata scheme needs to be figured out.
 ** Theoretically, this could be implemented in host and device side as it
    doesn't change the side of the data being stored.
 ** Usually, encryption is used to improve the lifetime of flash as it provides
    entropy when writing to flash.
 ** It is also be easy to put at the host-side.

* RAIN
 ** Handled at host-side.
  *** This is easily done in the block mapping scheme within the host.
  *** On a sidenote, RAIN could be implemented using the GlusterFS/Ceph
      distributed filesystems.

* Bad Block Mapping
 ** Hybrid approach.
  *** The device sends the bad block list at device start.
  *** The host may then handle failures and update the bad block list when
      writes starts to fail.
 ** In later iterations, there should also be some form of data scrubbing
    (rereading data and rewrite them whenever necessary) to prolong the life
    of the flash chips.  This depends on what kinds of chips that will be used.

* Lower level Flash optimizations of course fit in the HW.

